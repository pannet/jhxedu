<?php

namespace addons\JhxEdu\html5\assets;

use yii\web\AssetBundle;

/**
 * 静态资源管理
 *
 * Class AppAsset
 * @package addons\JhxEdu\html5\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@addons/JhxEdu/html5/resources/';

    public $css = [
    ];

    public $js = [
    ];

    public $depends = [
    ];
}