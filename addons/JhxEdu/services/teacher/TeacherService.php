<?php

namespace addons\JhxEdu\services\teacher;

use addons\JhxEdu\common\enums\AccessTokenGroupEnum;
use common\helpers\EchantsHelper;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use common\helpers\ArrayHelper;
use common\components\Service;
use common\enums\WhetherEnum;
use common\enums\StatusEnum;
use common\helpers\StringHelper;
use common\helpers\BcHelper;
use common\helpers\AddonHelper;
use addons\JhxEdu\common\models\SettingForm;
use addons\JhxEdu\common\models\teacher\Teacher;

/**
 * Class ProductService
 * @package addons\JhxEdu\services\product
 * @author jianyan74 <751393839@qq.com>
 */
class TeacherService extends Service
{
    
    /**
     * @param ProductSearch $search
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getListBySearch(Teacher $search)
    {
        $order = ['sort asc', 'id desc'];
        $data = Teacher::find()
            ->where(['status' => StatusEnum::ENABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->andFilterWhere(['like', 'name', $search->keyword]);
       
        $pages = new Pagination([
            'totalCount' => $data->count(),
            'pageSize' => $search->page_size,
            'validatePage' => false,
        ]);
        $models = $data->offset($pages->offset)
            ->orderBy(implode(',', $order))
            ->select([
                'id',
                'name',
                'avatar',
                'school',
                'level',
                'intro'
            ])
            ->asArray()
            ->limit($pages->limit)
            ->all();
  
        return $models;
    }
    

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findById($id)
    {
        return Teacher::find()
            ->where(['id' => $id, 'status' => StatusEnum::ENABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->one();
    }
    /**
     * @param $id
     * @param $member_id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    public function findViewById($id, $member_id)
    {
        $model = Teacher::find()
            ->where(['id' => $id])
            ->andWhere(['>=', 'status', StatusEnum::DISABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->with(['data'])
            ->asArray()
            ->one();
        if (!$model) {
            throw new NotFoundHttpException('找不到该教师');
        }
        return $model;
    }
  
    public function contentCount($product_id){
       return  ProductContent::find()->where(['status'=>1,'product_id'=>$product_id])->count();
    }
    /**
     * @param $ids
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findByIds($ids)
    {
        return Product::find()
            ->where(['status' => StatusEnum::ENABLED, 'product_status' => StatusEnum::ENABLED])
            ->andWhere(['in', 'id', $ids])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->orderBy('id asc')
            ->all();
    }
}