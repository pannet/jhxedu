<?php

namespace addons\JhxEdu\services\member;

use common\enums\StatusEnum;
use common\helpers\EchantsHelper;
use common\models\member\Member;
use common\components\Service;

/**
 * Class MemberService
 * @package addons\JhxEdu\services\member
 */
class MemberService extends Service
{
    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findById($id, $select = ['*'])
    {
        return Member::find()
            ->select($select)
            ->where(['id' => $id, 'status' => StatusEnum::ENABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->one();
    }
    /*
     * 是否是vip
     */
    public function isVip($userInfo){
        if(!$userInfo || !$userInfo->vip_expire || !$userInfo->is_vip){
            return false;
        }
        $now = time();
        if($userInfo->is_vip == 1 && $now < $userInfo->vip_expire){
            return true;
        }
        return false;
    }
}