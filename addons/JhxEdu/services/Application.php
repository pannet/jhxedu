<?php

namespace addons\JhxEdu\services;

use common\components\Service;

/*
 *
 * @author jianyan74 <751393839@qq.com>
 */
class Application extends Service
{
    /**
     * @var array
     */
    public $childService = [
    	// ------------------------ 产品 ------------------------ //

        'product' => 'addons\JhxEdu\services\product\ProductService',
        'productContent' => 'addons\JhxEdu\services\product\ProductContentService',
        'productCate' => 'addons\JhxEdu\services\product\CateService',
        // ------------------------ 公用 ------------------------ //
        'adv' => 'addons\JhxEdu\services\common\AdvService',
        'collect' => 'addons\JhxEdu\services\common\CollectService',
        // ------------------------ 会员 ------------------------ //
        'member' => 'addons\JhxEdu\services\member\MemberService',
        'memberFootprint' => 'addons\JhxEdu\services\member\FootprintService',
        // ------------------------ 订单 ------------------------ //
        'order' => 'addons\JhxEdu\services\order\OrderService',
        // ------------------------ 教师 ------------------------ //
        'teacher' => 'addons\JhxEdu\services\teacher\TeacherService',
        'liveCate' => 'addons\JhxEdu\services\live\CateService',
    ];
}