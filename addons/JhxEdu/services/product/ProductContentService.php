<?php

namespace addons\JhxEdu\services\product;
use addons\JhxEdu\common\enums\AccessTokenGroupEnum;
use common\helpers\EchantsHelper;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use common\helpers\ArrayHelper;
use common\components\Service;
use common\enums\WhetherEnum;
use common\enums\StatusEnum;
use common\helpers\StringHelper;
use common\helpers\BcHelper;
use common\helpers\AddonHelper;
use addons\JhxEdu\common\models\product\ProductContent;

/**
 * Class ProductService
 * @package addons\JhxEdu\services\product
 */
class ProductContentService extends Service
{
    /**
     * @param ProductSearch $search
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getListBySearch(ProductContent $search)
    {
        $order = ['sort asc', 'id desc'];
        $data = ProductContent::find()
            ->where(['status' => StatusEnum::ENABLED])
            ->andFilterWhere(['product_id' => $search->product_id]);
        $pages = new Pagination([
            'totalCount' => $data->count(),
            'pageSize' => $search->page_size,
            'validatePage' => false,
        ]);
        $models = $data->offset($pages->offset)
            ->orderBy(implode(',', $order))
            ->select([
                'id',
                'name',
                'view',
                'price_type',
                'type',
                'pre_time',
                'created_at'
            ])
            ->asArray()
            ->limit($pages->limit)
            ->all();
        return $models;
    }
    /**
     * @param string $keyword
     * @return array
     */
    public function getList($keyword = '')
    {
        $data = Product::find()
            ->where(['status' => StatusEnum::ENABLED, 'product_status' => StatusEnum::ENABLED])
            ->andFilterWhere(['like', 'name', $keyword])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()]);
        $pages = new Pagination(['totalCount' => $data->count(), 'pageSize' => 10]);
        $models = $data->offset($pages->offset)
            ->orderBy('sort asc, id desc')
            ->select(['id', 'name','real_price'])
            ->asArray()
            ->limit($pages->limit)
            ->all();

        foreach ($models as &$model) {
            $model['covers'] = unserialize($model['covers']);
        }
        return [$models, $pages];
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findById($id)
    {
        return Product::find()
            ->where(['id' => $id, 'status' => StatusEnum::ENABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->one();
    }
    /**
     * @param $id
     * @param $member_id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    public function findViewById($id, $member_id)
    {
        $model = ProductContent::find()
            ->where(['id' => $id])
            ->andWhere(['status'=>1])
            ->asArray()
            ->with('data')
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('找不到该章节');
        }
        return $model;
    }
    /**
     * @param $ids
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findByIds($ids)
    {
        return Product::find()
            ->where(['status' => StatusEnum::ENABLED, 'product_status' => StatusEnum::ENABLED])
            ->andWhere(['in', 'id', $ids])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->orderBy('id asc')
            ->all();
    }
}