<?php

namespace addons\JhxEdu\backend\modules\common\controllers;

use Yii;
use common\enums\StatusEnum;
use common\models\base\SearchModel;
use common\traits\MerchantCurd;
use addons\JhxEdu\backend\controllers\BaseController;
use addons\JhxEdu\common\models\product\Product;
use addons\JhxEdu\common\models\product\Cate;
use yii\data\ActiveDataProvider;
/**
 * 幻灯片
 *
 * Class AdvController
 * @package addons\JhxEdu\backend\modules\common\controllers
 */
class LinkController extends BaseController
{
    use MerchantCurd;

    /**
     * @var Adv
     */
    public $modelClass = Adv::class;
    public function actionIndex($type)
    {
        switch ($type){
            case 'use':
                return $this->render('index', [
                'type'=>$type,
                ]);
                
                break;
           case 'product':
                $searchModel = new SearchModel([
                    'model' => Product::class,
                    'scenario' => 'default',
                    'partialMatchAttributes' => ['name'], // 模糊查询
                    'defaultOrder' => [
                        'sort' => SORT_ASC,
                        'id' => SORT_DESC,
                    ],
                    'pageSize' => 15,
                ]);

                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->query
                    ->andWhere(['status' => StatusEnum::ENABLED])
                    ->with(['cate']);

                return $this->render($this->action->id, [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'cates' => Yii::$app->jhxEduService->productCate->getMapList(),
                    'type'=>$type,
                ]);
               break;
           case 'cate':
               $dataProvider = new ActiveDataProvider([
                   'query' => Cate::find()->where(['status'=>1])->orderBy(['sort'=> SORT_ASC]),
                   'pagination' => [
                   'pageSize' =>200,
               ],
               ]);
               return $this->render($this->action->id, [
               'dataProvider'=>$dataProvider,
               'type'=>$type,
               ]);
               break;
           case 'article':
               $searchModel = new ArticleSearch();
               $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
               return $this->render('/nav/site/url', [
                   'searchModel' => $searchModel,
                   'dataProvider' => $dataProvider,
                   'type'=>$type,
               ]);
               break;
           case 'shop':
                   $searchModel = new ShopSearch();
                   $params = Yii::$app->request->queryParams;
                   unset($params['type']);
                   $dataProvider = $searchModel->search($params);
                   return $this->render('/nav/site/url', [
                       'searchModel' => $searchModel,
                       'dataProvider' => $dataProvider,
                       'type'=>$type,
                   ]);
               break;
           case 'shop_cate':
               $searchModel = new ShopCategorySearch();
               $params = Yii::$app->request->queryParams;
               unset($params['type']);
               $dataProvider = $searchModel->search($params);
               return $this->render('/nav/site/url', [
                   'searchModel' => $searchModel,
                   'dataProvider' => $dataProvider,
                   'type'=>$type,
               ]);
               break;
        }
    }
    
}