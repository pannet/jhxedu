<?php
namespace addons\JhxEdu\backend\modules\common\controllers;

use addons\JhxEdu\common\models\common\Nav;
use addons\JhxEdu\common\models\common\NavItem;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use addons\JhxEdu\backend\controllers\BaseController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use common\traits\MerchantCurd;
/**
 * NavItemController implements the CRUD actions for NavItem model.
 */
class NavItemController extends BaseController
{
    use MerchantCurd;
     /**
     * @var Adv
     */
    public $modelClass = NavItem::class;

    public function getViewPath()
    {
        return $this->module->getViewPath() . DIRECTORY_SEPARATOR . 'nav/item';
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','get'],
                ],
            ],
        ];
    }
    /**
     * Lists all NavItem models.
     * @return mixed
     */
    public function actionIndex($nav_id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NavItem::find()->where(['nav_id'=>$nav_id])->andWhere(['>','status',-1])->orderBy(['sort'=>SORT_ASC])
        ]);
        return $this->render('index', [
            'nav_id' => $nav_id,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Creates a new NavItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($nav_id)
    {
        $model = new NavItem();
        $nav = Nav::findOne($nav_id);
        if (!$nav) {
            throw new HttpException(400);
        }
        $model->nav_id =  $nav->id;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', '添加成功');
                return $this->redirect(['index', 'nav_id' => $model->nav_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'nav' => $nav,
        ]);
    }
    /**
     * Updates an existing NavItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', '更新成功');
            return $this->redirect(['index', 'nav_id' => $model->nav_id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing NavItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('success', '删除成功');
            return $this->redirect(['index', 'nav_id'=>$model->nav_id]);
        };
    }
    /**
     * Finds the NavItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NavItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = NavItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}