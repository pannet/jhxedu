<?php

namespace addons\JhxEdu\backend\modules\common\controllers;

use addons\JhxEdu\common\models\common\MemberVip;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use addons\JhxEdu\merchant\controllers\BaseController;
use yii\web\NotFoundHttpException;

/**
 * NavController implements the CRUD actions for Nav model.
 */
class MemberVipController extends BaseController
{

    /**
     * Lists all Nav models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MemberVip::find()
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * 编辑/创建
     *
     * @return mixed
     */
    public function actionEdit()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', '添加成功');
                return $this->redirect(['index']);
            }
        }
        //var_dump($modelData);die;
        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Nav model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nav model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nav the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (empty($id) || empty(($model = MemberVip::find()->where(['id' => $id])->andWhere(['merchant_id' => $this->getMerchantId()])->one()))) {
            $model = new MemberVip();
            $model->merchant_id = $this->getMerchantId();
            $model->loadDefaultValues();
        }

        return $model;
    }
}
