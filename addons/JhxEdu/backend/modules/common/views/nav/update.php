<?php
use yii\grid\GridView;
use common\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\Nav */
$this->title = '修改导航: ' . $model->key;
$this->params['breadcrumbs'][] = ['label' => '导航', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改导航';
?>
<div class="row">
    <div class="col-md-10">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
