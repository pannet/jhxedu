<?php

use yii\grid\GridView;
use common\helpers\Html;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '导航项列表';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title; ?></h3>
        <div class="box-tools">
            <?= Html::create(['create','nav_id'=>$nav_id]); ?>
        </div>
    </div>
    <div class="box-body">
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'title',
            'url',
            [
                'attribute' => 'sort',
                'filter' => false, //不显示搜索框
                'value' => function ($model) {
                    return Html::sort($model->sort);
                },
                'format' => 'raw',
                'headerOptions' => ['class' => 'col-md-1'],
            ],
            [
                'header' => "操作",
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit} {status} {delete}',
                'buttons' => [
                    'edit' => function ($url, $model, $key) {
                        return Html::edit(['edit', 'id' => $model->id]);
                    },
                    'status' => function ($url, $model, $key) {
                        return Html::status($model->status);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::delete(['delete', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>
    </div>
</div>
