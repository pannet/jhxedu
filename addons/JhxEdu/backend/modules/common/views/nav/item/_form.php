<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\webuploader\Files;
/* @var $this yii\web\View */
/* @var $model common\models\NavItem */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">基本信息</h3>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="col-lg-12">
                    <?= $form->field($model, 'title') ?>

                    <?= $form->field($model, 'url')->textarea(['maxlength' => 1024])->hint('') ?>
                    <a onclick="selectUrl()" class="ncap-btn"><i class="fa fa-search"></i>选择跳转链接</a><br/><br/>
                    <?= $form->field($model, 'icon')->widget(Files::class, [
                        'config' => [
                            'pick' => [
                                'multiple' => false,
                            ]
                        ]
                    ])->hint("建议大小： 高100px  宽100px"); ?>
                    <?= $form->field($model, 'target')->checkbox() ?>

                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' :  '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>
     <?php $this->beginBlock('url') ?>  
    function selectUrl(){
        var url = "/backend/jhx-edu/common/link/index?type=use";
        layer.open({
            type: 2,
            title: '选择链接',
            shadeClose: true,
            shade: 0.3,
            area: ['90%', '98%'],
            content: url,
        });
    }
    
    function call_back(url){
    $("#navitem-url").val(url);
    layer.closeAll('iframe');
    }
    
    <?php $this->endBlock() ?>  
    <?php $this->registerJs($this->blocks['url'], \yii\web\View::POS_END); ?>
