<?php

use yii\grid\GridView;
use common\helpers\Html;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '导航';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginBlock('content-header') ?>
<?= $this->title . ' ' . Html::a('新导航', ['create'], ['class' => 'btn btn-primary btn-flat btn-xs']) ?>
<?php $this->endBlock() ?>
<div class="box box-primary">
    <div class="box-body">


<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [

        'id',
        'key',
        'title',
        [
            'header' => "操作",
            'class' => 'yii\grid\ActionColumn',
            'template' => '{edit} {item}',
            'buttons' => [
                'edit' => function ($url, $model, $key) {
                    return Html::edit(['edit', 'id' => $model->id]);
                },
                'item' => function ($url, $model, $key) {
                    return Html::linkButton(['nav-item/index', 'nav_id' => $model->id],'导航项');
                },
            ],
        ],
    ],
]); ?>
    </div>
</div>
