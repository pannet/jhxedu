<?php
use yii\helpers\Html;
?>
<style>
.active_status{
color: blue;
}
</style>
 <div class="btn-toolbar" role="toolbar">
    	<div class="btn-group">
    		<button type="button" value="/pages/index/index" class="btn btn-default">首页</button>
    	</div>
      <div class="btn-group">
        <button type="button" value="/pages/user/user" class="btn btn-default">个人中心</button>
      </div>
    	<div class="btn-group">
    		<button type="button" value="/pages/user/vipbuy" class="btn btn-default">开通VIP</button>
    	</div>
    	<div class="btn-group">
    		<button type="button" value="/pages/product/list" class="btn btn-default">全部课程列表</button>
    	</div>
      <div class="btn-group">
        <button type="button" value="/pages/product/search?real_price=0" class="btn btn-default">免费课程列表</button>
      </div>
      <div class="btn-group">
        <button type="button" value="/pages/user/distribute" class="btn btn-default">分销记录</button>
      </div>
    	<!-- <div class="btn-group">
    		<button type="button" value="/pages/coupon/lists" class="btn btn-default">优惠券列表</button>
    	</div> -->
    	
    	<!-- <div class="btn-group">
    		<button type="button" value="/pages/video/list" class="btn btn-default">直播</button>
    	</div>
    	<div class="btn-group">
    		<button type="button" value="/pages/live/list" class="btn btn-default">短视频</button>
    	</div> -->
    	<!-- <div class="btn-group">
    		<button type="button" value="/pages/product/list?is_group=1" class="btn btn-default">拼团商品</button>
    	</div> -->
    </div>
<div style="margin-left: 43%" class="form-group save-box">
              <a style="width: 200px; margin-top: 90%;" type="submit" class="btn btn-primary btn-flat" onclick="select_member();">确定</a>              
    </div> 
<?php $this->beginBlock('specification') ?>  
    var url ='';
   $("button[type='button']").click(function(){
   		url = $(this).val();
   		
   		removeAllClass();
   		if($(this).hasClass("btn-default")){
       		$(this).removeClass("btn-default");
       		$(this).addClass("btn-info");
       		
   		}else if($(this).hasClass("btn-info")){
   			$(this).removeClass("btn-info");
       		$(this).addClass("btn-default");
   		}
   });
   
   //移除所有的样式
   function removeAllClass(){
       $("button").each(function(i){
            $("button:eq("+i+")").attr("class","btn btn-default");
       });
   }
   
    function select_member(){
        if (!url) {
           alert('请选择链接'); 
           return false;
        }
        window.parent.call_back(url);
    }   
 <?php $this->endBlock() ?> 
 <?php $this->registerJs($this->blocks['specification'], \yii\web\View::POS_END); ?>