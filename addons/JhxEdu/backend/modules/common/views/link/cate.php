<?php
use yii\helpers\Html;
use jianyan\treegrid\TreeGrid;
?>
 <div class="box box-primary">
        <div class="box-body">
        <?= TreeGrid::widget([
                    'dataProvider' => $dataProvider,
                    'keyColumnName' => 'id',
                    'parentColumnName' => 'pid',
                    'parentRootValue' => '0', //first parentId value
                    'pluginOptions' => [
                        'initialState' => 'collapsed',
                    ],
                    'options' => ['class' => 'table table-hover','id' => 'grid',],
                    'columns' => [
                        [
                            'class' => 'yii\grid\RadioButtonColumn',
                        ],
                        'id',
                        [
                            'attribute' => 'title',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $column) {
                                $str = Html::tag('span', $model->title, [
                                    'class' => 'm-l-sm',
                                ]);
                                return $str;
                            },
                        ],
                    ],
                ]); ?>
            
        </div>
    </div>
 
	<div style="margin-left: 43%" class="form-group save-box">
              <button style="width: 200px;" type="submit" class="btn btn-primary btn-flat" onclick="select_member();">确定</button>              
    </div>
 <?php $this->beginBlock('specification') ?>  
    var url ='';
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
       	var id = $(this).val();
        	url ="/pages/product/list?id="+id ;
        })
    })
    
    function select_member(){
      var input = $("input[type='radio']:checked");
        if (input.length == 0) {
           alert('请选择分类'); 
           return false;
        }
        window.parent.call_back(url);
    }   
 <?php $this->endBlock() ?> 
 <?php $this->registerJs($this->blocks['specification'], \yii\web\View::POS_END); ?>