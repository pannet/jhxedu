<?php
use common\helpers\Tree;
use common\models\CategoryModel;
use common\models\ProductCategory;
use common\models\ProductType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
?>
<div class="box box-primary">
        <div class="box-body"><div class="article-search">

    

</div></div>
    </div>
    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                //重新定义分页样式
                'tableOptions' => [
                    'class' => 'table table-hover rf-table',
                    'fixedNumber' => 3,
                    'fixedRightNumber' => 1,
                ],
                'options' => [
                    'id' => 'grid',
                ],
                'columns' => [
                    [
                        'class' => 'yii\grid\RadioButtonColumn',
                    ],
                    'id',
                   
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'attribute' => 'sale',
                    ],
                    [
                        'attribute' => 'virtual_sale',
                    ],
                    [
                        'label' => '销售价格',
                        'attribute' => 'real_price',
                    ],
                    [
                        'attribute' => 'vip_price',
                    ],
                    [
                        'attribute' => 'fenxiao_money',
                    ],
                    [
                        'attribute' => 'cate.title',
                        'label' => '课程分类',
                        'filter' => Html::activeDropDownList($searchModel, 'cate_id', $cates, [
                                'prompt' => '全部',
                                'class' => 'form-control',
                            ]
                        ),
                        'format' => 'raw',
                        'headerOptions' => ['class' => 'col-md-1'],
                    ],
                ],
            ]); ?>
        </div>
        <input id="copyInput" type="text" style="display: none"> 
    </div>  
    
     <div style="margin-left: 43%" class="form-group save-box">
              <button style="width: 200px;" type="submit" class="btn btn-primary btn-flat" onclick="select_member();">确定</button>              
        </div>
 <?php $this->beginBlock('specification') ?>  
    var url ='';
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
       	    var id = $(this).val();
        	url = "/pages/product/detail?id="+id ;
        })
    })
    function select_member(){
      var input = $("input[type='radio']:checked");
        if (input.length == 0) {
           alert('请选择商品'); 
           return false;
        }
        window.parent.call_back(url);
    }   
 <?php $this->endBlock() ?> 
 <?php $this->registerJs($this->blocks['specification'], \yii\web\View::POS_END); ?>
 