<?php
//use common\assets\LayerAsset;
use yii\helpers\Html;
//LayerAsset::register($this);
$this->title = '请选择链接';
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body data-spy="scroll" data-target="#myScrollspy">
<div class="col-md-2">
	<?= $this->render('left', [
	    'type'=>$type
    ]) ?>
</div>
<div class="col-md-10">
<?php if ($type=='use'): ?>

   <?= $this->render($type, [
    ]) ?>
    
<?php elseif ($type=='product'): ?>

 <?= $this->render($type, [
    'cates'=>$cates,
     'searchModel'=>$searchModel,
     'dataProvider'=>$dataProvider
    ]) ?>
    
<?php elseif ($type=='cate'): ?>

 <?= $this->render($type, [
     'dataProvider'=>$dataProvider
    ]) ?>
    
<?php elseif ($type=='article'): ?>

 <?= $this->render($type, [
     'dataProvider'=>$dataProvider
    ]) ?>
    
 <?php elseif ($type=='shop'): ?>

 <?= $this->render($type, [
     'model'=>$searchModel,
     'dataProvider'=>$dataProvider
    ]) ?>

<?php elseif ($type=='shop_cate'): ?>

 <?= $this->render($type, [
     'model'=>$searchModel,
     'dataProvider'=>$dataProvider
    ]) ?>
<?php endif; ?>
</div>
</body>
</html>
