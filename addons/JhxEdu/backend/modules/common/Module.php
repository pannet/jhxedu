<?php

namespace addons\JhxEdu\backend\modules\common;

/**
 * Class Module
 * @package addons\JhxEdu\merchant\modules\common
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\backend\modules\common\controllers';

    public function init()
    {
        parent::init();
    }
}