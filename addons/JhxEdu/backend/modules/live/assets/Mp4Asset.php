<?php
namespace common\modules\live\assets;

use yii\web\AssetBundle;


class Mp4Asset extends AssetBundle
{
    public $sourcePath = '@common/modules/live/static';
    public $js = [
        'hls.min.js',
    ];

    public $css = [
    ];

 /*    public $depends = [
        'yii\web\JqueryAsset'
    ]; */
}
