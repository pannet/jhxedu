<?php
namespace common\modules\live;
use common\components\PackageInfo;
class ModuleInfo extends \common\modules\ModuleInfo
{
	public $isCore = 0;
	public $menu=[
       'name'=>'直播带货', //菜单名称
       'route'=>'/live/default/index',//路由地址
       'icon'=>'fa-tv', //图标
       'order'=>99   //排序
	];
    public $info = [
        'author' => 'wsyone',
        'bootstrap' => 'backend|api|frontend',
        'version' => 'v1.0',
        'id' => 'live',
        'name' => '直播带货',
        'description' => '微信小程序直播带货插件'
    ];
}