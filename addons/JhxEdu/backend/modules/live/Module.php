<?php

namespace addons\JhxEdu\backend\modules\live;

/**
 * Class Module
 * @package addons\JhxEdu\merchant\modules\live
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\backend\modules\live\controllers';

    public function init()
    {
        parent::init();
    }
}