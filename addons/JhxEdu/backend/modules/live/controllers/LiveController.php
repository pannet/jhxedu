<?php
/**
 * 
 * 直播带货
 * Author wsyone wsyone@faxmail.com
 * Time:2020年5月13日上午10:21:56
 * Copyright:钦州市友加信息科技有限公司
 * site:https://www.jihexian.com
 */
namespace addons\JhxEdu\backend\modules\live\controllers;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use api\common\controllers\XcxApi;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\UploadedFile;
use common\models\LiveCategory;
use yii\data\ActiveDataProvider;
use common\models\LiveProduct;
use yii\helpers\ArrayHelper;
use common\models\LiveRelation;
use function Qiniu\json_decode;
use addons\JhxEdu\common\models\live\Live;
use addons\JhxEdu\common\models\live\LiveSearch;
/**
 * Default controller for the `live` module
 */
class LiveController extends Controller
{
    
    public function actions()
    {
        return [
            'ajax-update-field' => [
                'class' => 'common\\actions\\AjaxUpdateFieldAction',
                'allowFields' => ['status'],
                'findModel' => [$this, 'findModel']
            ],
            'switcher' => [
                'class' => 'backend\widgets\grid\SwitcherAction'
            ],
            'position' => [
                'class' => 'backend\\actions\\Position',
                'returnUrl' => Url::current()
            ]
        ];
    }
    
    /**
     * Lists all Live models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiveSearch();
        $param=Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($param);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGetCode($roomId) {
        $xcxApi = new XcxApi();
        $data = $xcxApi->getLiveCode($roomId);
        if(isset($data['errcode'])&&$data['errcode']!=0){
            throw new NotFoundHttpException('直播已经过期');
        }
        return $this->render('wx_code', [
            'data'=>$data
        ]);
    }
    
    public function actionRecycle()
    {
        $searchModel = new LiveSearch();
        $param=Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($param,'recycle');
        return $this->render('recycle', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Live model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionMp4($id)
    {
        $model=$this->findModel($id);
        if($model['live_status']!=103||empty($model->live_replay)){
            yii::$app->session->setFlash('error','暂无数据');
            return $this->goBack();
        }
        return $this->render('mp4', [
            'model' =>Json::decode($model->live_replay)
        ]);
    }
    /**
     * Creates a new Live model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Live();
        if (yii::$app->request->isPost){
            $model->load(Yii::$app->request->post());
            //$cover = $model->getAttributes();
            $share_img = Yii::$app->request->post()['Live']['share_img']['path'];
            if(!$share_img){
                yii::$app->session->setFlash('error','请上传封面');
               // $this->goBack();
            }
            $model->type ='url';
            $model->share_img = $share_img;
            $model->live_status=0;
            $url = Yii::$app->request->post()['Live']['url']['path'];
            $model->url=$url;
            if($model->save()){
                yii::$app->session->setFlash('success','创建成功');
            }else{
                yii::$app->session->setFlash('error',current($model->getFirstErrors()));
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreateLive()
    {    $request = Yii::$app->request;
        $id = $request->get('id');
        $tran = Yii::$app->db->beginTransaction();
        $model = new Live();
        $xcxApi=new XcxApi();
        if ($model->load(Yii::$app->request->post())) {
           $data=$model->getAttributes();
           //上传封面和分享图片
           $model->cover_img = UploadedFile::getInstance($model, 'cover_img');
           $model->share_img = UploadedFile::getInstance($model, 'share_img');
           
           //$model->cover_img->tempName;
           //print_r(dirname(Yii::$app->BasePath));die;
           $newData = array();
           $start = strtotime($data['start_time']);
           $end = strtotime($data['end_time']);
           $model->start_time = $start;
           $model->end_time = $end;
           if($path = $model->upload()){
               //$pic1='@C:\web_app\yjshop\web\storage\logo.png';
               //拼接封面图片在服务器中的绝对路径
               $pic1='@'.dirname(Yii::$app->BasePath).DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'weixin'.DIRECTORY_SEPARATOR.$path['cover'];
               $image1=$xcxApi->uploadMedia($pic1,'image');//获取封面图片media_id
               $image1 = json_decode($image1,true);
               //拼接封面图片在服务器中的绝对路径
               $pic2='@'.dirname(Yii::$app->BasePath).DIRECTORY_SEPARATOR.'web'.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'weixin'.DIRECTORY_SEPARATOR.$path['share'];
               $image2=$xcxApi->uploadMedia($pic2,'image');//获取分享图片media_id
               $image2 = json_decode($image2,true);
               //构造小程序直播间api申请的json数据结构
               if(!$image1||!$image2){
                   yii::$app->session->setFlash('error','直播封面或分享图片格式错误');
               }else {
                   $newData['shareImg']=$image2['media_id'];
                   $newData['coverImg']=$image1['media_id'];
                   $newData['name']=$data['name'];
                   $newData['startTime']=$start;
                   $newData['endTime']=$end;
                   $newData['anchorName']=$data['anchor_name'];
                   $newData['anchorWechat']=$data['anchor_wechat'];
                   $newData['type']=$data['type'];
                   $newData['screenType']=$data['screen_type'];
                   $newData['closeLike']=$data['close_like'];
                   $newData['closeGoods']=$data['close_goods'];
                   $newData['closeComment']=$data['close_comment'];
                   
                   //检验时间是否有效
                   $newTime1=$start-time();
                   if($newTime1<0){
                       yii::$app->session->setFlash('error','开始时间不能在当前时间之前');
                   }else if($newTime1<600){
                       yii::$app->session->setFlash('error','开始时间必须大于当前时间的10分钟');
                   }
                   $newTime2=$end-$start;
                   if($newTime2<=1800){
                       yii::$app->session->setFlash('error','直播时间必须大于30分钟');
                   }else if($newTime2>86400){
                       yii::$app->session->setFlash('error','直播时间不能大于24小时');
                   }else{
                       //开始创建直播间
                       $msg = $xcxApi->AddLive($newData);
                       //print_r($msg);die;
                       //房间申请成功后往直播表中插入数据
                       if($msg['errcode']==0&&!empty($msg['roomId'])){
                           $model->roomid=$msg['roomId'];
                           $model->live_status=102;
                           $model->url='noUrl';
                           $model->type='live';
                           $model->cover_img =Yii::$app->request->hostInfo.'/storage/weixin/'.$path['cover'];
                           $model->share_img = Yii::$app->request->hostInfo.'/storage/weixin/'.$path['share'];
                           if($model->save()){
                               $tran->commit();
                               yii::$app->session->setFlash('success','创建成功');
                           }else{
                               $tran->rollBack();
                               yii::$app->session->setFlash('error',current($model->getFirstErrors()));
                           }
                           return $this->redirect(['index']);
                       }elseif ($msg['errcode']==300036){
                           yii::$app->session->setFlash('error','主播微信号不合法');
                       }elseif($msg['errcode']==300034){
                           yii::$app->session->setFlash('error','主播名字不合法');
                       }elseif($msg['errcode']==300002){
                           yii::$app->session->setFlash('error','直播间标题不合法');
                       }else{
                           yii::$app->session->setFlash('error','创建失败');
                       }
                   }
               }
           }else{
               yii::$app->session->setFlash('error','直播封面和分享图上传失败');
           }
        }
        
        if($model->start_time&&$model->end_time){
            $model->start_time = $data['start_time'];
            $model->end_time = $data['end_time'];
        }
        return $this->render('create_live', [
            'model' => $model,
            'dropDown' => Yii::$app->jhxEduService->liveCate->getDropDownForEdit($id),
        ]);
    }
    
    /**
     * Updates an existing Live model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post())) {
            $share_img = Yii::$app->request->post()['Live']['share_img']['path'];
            $url = Yii::$app->request->post()['Live']['url']['path'];
            $model->url=$url;
            if(!$share_img){
                yii::$app->session->setFlash('error','请上传封面');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
                $model->share_img = $share_img;
                if($model->save()){
                    yii::$app->session->setFlash('success','修改成功');
                }else{
                    yii::$app->session->setFlash('error',current($model->getFirstErrors()));
                }
          
            return $this->redirect(['index']);
        } else {
            $model->start_time = date('Y-m-d h:i:s',$model->start_time);
            $model->end_time = date('Y-m-d h:i:s',$model->end_time);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionEditCate($id,$cate_id){
        $live = $this->findModel($id);
        $live->cate_id = $cate_id;
        if($live->save()){
            Yii::$app->session->setFlash('success','修改分类成功');
        }else{
            Yii::$app->session->setFlash('error','修改分类失败');
        }
        return $this->redirect(['index']);
    }
    public function actionRealDelete($id){
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success','删除成功');
        return $this->redirect(['recycle']);
    }
    
    public function actionUpdateLive(){
        $model=new XcxApi();
        $info=$model->UpdateLive();
        if($info['status']==1){
          Yii::$app->session->setFlash('success','同步成功');
        }else{
            Yii::$app->session->setFlash('error',$info['msg']);
        }
        return $this->redirect(['index']);
    }
    
    /*
            * 已经在直播房间的商品
     */
    public function actionImport($id){
        $room = $this->findModel($id);
        $searchModel = new LiveProductSearch();
        $params = Yii::$app->request->queryParams;
        $product = LiveRelation::find()->where(['live_id'=>$id,'room_id'=>$room->roomid])->all();
        $ids = ArrayHelper::getColumn($product,'product_id');
        $dataProvider = $searchModel->search($params,$ids,'import');
        return $this->render('import/index',[
            'searchModel'=>$searchModel,
            'dataProvider'=>$dataProvider,
            'room'=>$room
        ]);
    }
   
    public function actionAddProduct($id){
        $params = Yii::$app->request->get();
        $xcxApi=new XcxApi();
        $post_data = [];
        $liveProduct = LiveProduct::findAll(['id'=>json_decode($params['ids'])]);
        $goods_ids = ArrayHelper::getColumn($liveProduct,'goods_id');
        $post_data['ids'] = $goods_ids;
        $post_data['roomId'] = $params['room_id'];
        $msg = $xcxApi->addLiveProduct($post_data,'import');
        //print_r($msg);die;
        if($msg['errcode']==0){
            foreach ($liveProduct as $lepr){
            $lra = new LiveRelation();
            $lra->room_id = $params['room_id'];
            $lra->product_id = $lepr['id']; 
            $lra->live_id = $id; 
            $lra->goods_id = $lepr['goods_id'];
            if($lra->save()){
                yii::$app->session->setFlash('success','添加成功');
            }else{
                yii::$app->session->setFlash('error','添加失败');
            }
           }
        }else if($msg['errcode']==300022||$msg['errcode']==300023){
            yii::$app->session->setFlash('error','直播房间已经失效');
        }else if($msg['errcode']==300025) {
            yii::$app->session->setFlash('error','该商品审核未通过');
        }
        return $this->redirect(['import','id'=>$id]);
    }
    
    /**
     * @param int $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->softDelete();
        yii::$app->session->setFlash('success','删除成功');
        return $this->redirect(['index']);
    }
    
    /**
     * @param int $id
     * @return mixed
     */
    public function actionReset($id)
    {
        $model = $this->findModel($id);
        $model->is_del = 0;
        if($model->save()){
            yii::$app->session->setFlash('success','恢复成功');
        }
        return $this->redirect(['index']);
    }
    
    
    /**
     * Finds the Live model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Live the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        if (($model = Live::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /*
     * 小龙
     * 增加直播分类的个数
     * 
     * */
    public function addCategoryLive($cate_id){
        $cate = LiveCategory::findOne(['id'=>$cate_id]);
        $cate->article +=1;
        $cate->save();
    }
}
