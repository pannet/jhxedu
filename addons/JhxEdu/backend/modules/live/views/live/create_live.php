<?php

use common\traits\Tree;
use addons\JhxEdu\common\models\live\LiveCategory;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Live */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-body">
   <?php $form = ActiveForm::begin([
       'action'=>'/admin/live/default/create-live',
       'options' => [
           'enctype' => 'multipart/form-data',
           'onsubmit'=>'return getPath()']
   ]) ?>
    <?= $form->field($model, 'name',
        ['inputOptions' =>[
            'placeholder' =>'直播标题至少三个汉字',
            'class'=>'form-control'
        ]])->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'cate_id')->dropDownList($dropDown) ?>
    
    <?= $form->field($model, 'anchor_name',['inputOptions' =>[
            'placeholder' =>'主播昵称至少两个汉字',
            'class'=>'form-control'
        ]])->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'anchor_wechat',['inputOptions' =>[
            'placeholder' =>'主播微信号必须要经过核实和实名认证，否则无法申请直播间',
            'class'=>'form-control'
        ]])->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'cover_img')->fileInput() ?>
    <div style="color:#fe7300;">直播间背景墙是每个直播间的默认背景。<br/>建议尺寸：1080像素 * 1920像素图片大小不得超过2M<br/>格式：jpg,png 否则无法创建直播间<br/><br/></div>
    <?= $form->field($model, 'share_img')->fileInput() ?>
    <div style="color:#fe7300;">观众在微信对话框内分享的直播间将以分享卡片的形式呈现。
<br/>建议尺寸：800像素 * 640像素图片大小不得超过1M<br/>格式：jpg,png 否则无法创建直播间<br/><br/></div>
	<?php // $form->field($model, 'cover_img')->textInput(['maxlength' => true]) ?>
	
	<?php // $form->field($model, 'share_img')->textInput(['maxlength' => true]) ?>
	
	<div class="form-group field-live-cover_img">
        <label class="control-label" for="live-cover_img">开播时间</label>
         <?= DateTimePicker::widget([
                'model' => $model,
                'attribute' => 'start_time',
                'options' => ['placeholder' => '开播时间必须在当前时间的10分钟后，不能在半年后'],
                'pluginOptions' => [
                        'format' => 'yyyy-mm-dd hh:ii:ss',
                ]
            ]);
            ?>
        </div>
<div class="form-group field-live-cover_img">
        <label class="control-label" for="live-cover_img">结束直播时间</label>    
     <?= DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'end_time',
        'options' => ['placeholder' => '直播时长至少要30分钟，不能超过24小时'],
        'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii:ss',
        ]
    ]);
    ?>
</div>   
    <?php // $form->field($model, 'sort')->textInput() ?>
    <?= $form->field($model, 'type')->dropDownList([0=>'直播',1=>'推流']); ?>
    <?= $form->field($model, 'screen_type')->dropDownList([0=>'横屏',1=>'竖屏']); ?>
    <?= $form->field($model, 'close_like')->dropDownList([0=>'开启',1=>'关闭']); ?>
    <?= $form->field($model, 'close_goods')->dropDownList([0=>'开启',1=>'关闭']); ?>
    <?= $form->field($model, 'close_comment')->dropDownList([0=>'开启',1=>'关闭']); ?>
    
    <div class="form-group">
        <?= Html::submitButton('保存',['class' =>'btn btn-success btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>

<?php $this->beginBlock('js_end') ?>
function getPath(){
var anchor_name = $('#live-anchor_name').val();
 if(!anchor_name){
     alert('请输入主播名');
  return false;
  }
  
var anchor_wechat = $('#live-anchor_wechat').val();
 if(!anchor_wechat){
     alert('请输入主播微信号');
  return false;
  }
  
var path1 = $('#live-cover_img').val();
  if(!path1){
     alert('请选择直播背景墙');
  return false;
  }
  
var path2 = $('#live-share_img').val();
  if(!path2){
     alert('选择分享卡片封面');
  return false;
  } 
var start = $('#live-start_time').val();
  if(!start){
     alert('请输入开始时间');
  return false;
  }
var end = $('#live-end_time').val();
if(!end){
     alert('请输入结束时间');
  return false;
}
}
<?php $this->endBlock(); ?>
<?php $this->registerJs($this->blocks['js_end'],\yii\web\View::POS_END);//将编写的js代码注册到页面底部 ?> 
