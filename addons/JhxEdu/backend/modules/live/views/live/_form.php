<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\webuploader\Files;

/* @var $this yii\web\View */
/* @var $model common\models\Live */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'share_img')->widget(Files::class, [
                        'type' => 'images',
                        'config' => [
                            //'compress'=>true,
                            'pick' => [
                                'multiple' => false,
                            ],
                            'formData' => [
                                 // 不配置则不生成缩略图
                                'thumb' => [
                                    [
                                        'width' => 600,
                                        'height' => 660,
                                    ]
                                ]
                            ]
                        ]
                    ])->hint("建议大小：  宽600px 高600px"); ?>

    
    <?php if (!$model->isNewRecord): ?>
    
    <?php if($model->type=='url'):?>
    	<?= $form->field($model, 'url')->textInput() ?>
    <?php else :?>
    	<?= $form->field($model, 'live_replay')->textarea() ?>
    <?php endif;?>
    <?php else :?>
    <?= $form->field($model, 'url')->textInput() ?>
    <?php endif;?>
    
    <?= $form->field($model, 'sort')->textInput() ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '创建' : '保存', ['class' => $model->isNewRecord ? 'btn btn-success btn-flat' : 'btn btn-primary btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
