<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '直播带货';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box box-primary">
    
        <div class="box-body">
            <?= "小程序直播" .' '
    . Html::a('新建视频', ['create'], ['class' => 'btn btn-primary btn-flat btn-xs']).' '
    . Html::a('新建直播间', ['create-live'], ['class' => 'btn btn-primary btn-flat btn-xs']).' '
    . Html::a('立即同步', ['update-live'], ['class' => 'btn btn-primary btn-flat btn-xs']).' '
    . Html::a('回收站', ['recycle'], ['class' => 'btn btn-danger btn-flat btn-xs']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
        'columns' => [
                    'id',
                    'name',
                  //  'roomid',
                [
                    'label' => '封面',
                    "format"=>'raw',
                    'value' => function($model) {
                        return Html::img($model->share_img,["width"=>"84","height"=>"84"]);
                    },
                    ],
                    'start_time:datetime',
                     'end_time:datetime',
                     
                     [
                         'label' => '直播状态',
                         "format"=>'raw',
                         'value' => function($model) {
                         return $model->get_status($model->live_status);
                         },
                         ],
                    // 'anchor_name',
                    // 'anchor_img',
                    // 'goods:ntext',
                    // 'live_replay:ntext',
                    // 'is_top',
                    // 'is_del',
                    // 'created_at',
                    // 'updated_at',
                       /*  [
                            'class' => 'backend\widgets\grid\SwitcherColumn',
                            'attribute' => 'status'
                        ],   */
                         [
                             'attribute' => 'type',
                             'value' => function ($model) {
                             return $model->type=='live'?'直播':'视频链接';
                             },
                         ],
                            [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{mp4} {view} {delete} {update}',
                            'buttons' => [
                                'mp4' => function($url, $model, $key) {
                                if(!empty($model->live_replay)){
                                   return  Html::a('<i class="fa fa-tv"></i>',Url::to(['mp4','id'=>$model->id]), ['class' => 'btn btn-xs btn-default','data-toggle' => 'tooltip']);
                                }
                                }
                             
                                ]
                                ],
                ],
            ]); ?>
        </div>
    </div>
