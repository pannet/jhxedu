<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Live */

$this->title = '新建视频';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="live-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
