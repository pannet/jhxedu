<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Live */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'roomid',
            [
                'label' => '封面图',
                "format"=>'raw',
                'value' => function($model) {
                return Html::img($model->share_img,["width"=>"84","height"=>"84"]);
                },
                ],
            [
                'label' => '背景图',
                "format"=>'raw',
                'value' => function($model) {
                return Html::img($model->cover_img,["width"=>"84","height"=>"84"]);
                },
                ],
                [
                    'label' => '直播状态',
                    "format"=>'raw',
                    'value' => function($model) {
                    return $model->get_status($model->live_status);
                    },
                    ],
            'start_time:datetime',
            'end_time:datetime',
            'anchor_name',
            [
                'label' => '回放内容',
                "format"=>'raw',
                'value' => function($model) {
                return $model->get_status($model->live_status);
                },
                ],
           // 'goods:ntext',
            //'live_replay:ntext',
            'created_at:datetime',
            'updated_at:datetime',
            'sort',
        ],
    ]) ?>
    </div>
</div>
