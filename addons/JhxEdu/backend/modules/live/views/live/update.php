<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Live */
?>
<div class="live-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
