<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\live\assets\Mp4Asset;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', '直播带货');
$this->params['breadcrumbs'][] = $this->title;
Mp4Asset::register($this);
?>

<script src="https://cdn.jsdelivr.net/hls.js/latest/hls.min.js"></script>
<?php $this->beginBlock('content-header') ?>
<?= $this->title . ' ' . Html::a(Yii::t('common', '新建记录'), ['create'], ['class' => 'btn btn-primary btn-flat btn-xs']). ' ' . Html::a(Yii::t('common', '立即同步'), ['update-live'], ['class' => 'btn btn-primary btn-flat btn-xs']) ?>
<?php $this->endBlock() ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box box-primary">
        <div class="box-body">
         <div class="box box-primary">
        <div class="box-body">
        <div id="w0" class="grid-view">
         <table class="table table-bordered table-hover table-responsive">
         	<thead>
		     <tr>
		     <th>ID</th>
			 <th>视频</th>
			 <th>直播时间</th>
			 <th>有效期</th>
			</tr>
		</thead>
		<tbody>
	    <?php if(is_array($model)):?>
		<?php foreach($model as $key=>$vo):?>
		<tr>
			<td><?=$key+1?></td>
			<?php if($key==0):?>
				<td><video id="video" width="100"></video></td>
				<?php $this->beginBlock('mp4') ?>  

  if(Hls.isSupported()) {
    var video = document.getElementById('video');
    var hls = new Hls();
    hls.loadSource('<?=$vo['media_url']?>');
    hls.attachMedia(video);
    hls.on(Hls.Events.MANIFEST_PARSED,function() {
      video.play();
  });
 }
           <?php $this->endBlock()?>
				
            <?php else:?>
			<td> <video src="<?=$vo['media_url']?>" width="100" ></video></td>
           <?php endif;?>
			<td><?=$vo['create_time']?></td>
			<td><?=$vo['expire_time']?></td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
		</tbody>
		</table>
    </div>       
     </div>
</div>
</div>
    </div>
    	<?php $this->registerJs($this->blocks['mp4'], \yii\web\View::POS_END); ?>
    