<?php

use yii\widgets\ActiveForm;
use common\widgets\webuploader\Files;
use common\enums\WhetherEnum;

$this->title = '参数设置';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php $form = ActiveForm::begin([]); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="false">系统设置</a></li>
                <li><a data-toggle="tab" href="#tab-2" aria-expanded="false">分销设置</a></li>
                <li><a data-toggle="tab" href="#tab-3" aria-expanded="false">分享设置</a></li>
                <li><a data-toggle="tab" href="#tab-4" aria-expanded="false">店铺设置</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                       
                        <?= $form->field($model, 'edu_pay_method')->dropDownList([
                            1 => '仅微信支付',
                            2 => '多种支付方式',
                        ])->hint('仅微信支付时会直接弹出微信支付框，多种支付方式会多一个选择支付方式步骤'); ?>
                        <?= $form->field($model, 'edu_account_type')->dropDownList([
                            1 => '仅公众号端',
                            2 => '多种终端',
                        ])->hint('【多种终端】会使用账号统一机制，【仅公众号】时微信号就是唯一账号。系统运行之初就应设定好，后期不可改变'); ?>
                        <?= $form->field($model, 'register_auth')->dropDownList([
                            1 => '允许',
                            2 => '不允许',
                        ])->hint('【允许】在注册时即可同时进行第三方授权'); ?>
                        <?= $form->field($model, 'register_require_pass')->dropDownList([
                            1 => '需要',
                            2 => '不需要',
                        ])->hint('【允许】在注册时必须填写密码。【不需要】无需填写密码，登录时只能用短息验证码登录'); ?>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <?= $form->field($model, 'edu_fenxiao')->dropDownList([
                            0 => '关闭',
                            1 => '开启',
                        ]); ?>
                        <?= $form->field($model, 'edu_fenxiao_type')->dropDownList([
                            1 => '商品分销',
                        ])->hint('商品分销：用户通过他人分享的链接购买课程即可获得佣金；二级分销：绑定人员关系'); ?>
                    </div>
                </div>
                <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <?= $form->field($model, 'share_title')->textInput()?>
                        <?= $form->field($model, 'share_desc')->textInput()?>
                        <?= $form->field($model, 'share_cover')->widget(Files::class, [
                            'type' => 'images',
                            'theme' => 'default',
                            'themeConfig' => [],
                            'config' => [
                                'pick' => [
                                    'multiple' => false,
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div id="tab-4" class="tab-pane">
                    <div class="panel-body">
                        <?= $form->field($model, 'shop_name')->textInput()?>
                        <?= $form->field($model, 'shop_address')->textInput()?>
                        <?= $form->field($model, 'shop_tel')->textInput()?>
                        <?= $form->field($model, 'shop_image')->widget(Files::class, [
                            'type' => 'images',
                            'theme' => 'default',
                            'themeConfig' => [],
                            'config' => [
                                'pick' => [
                                    'multiple' => false,
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="box-footer text-center">
                    <button class="btn btn-primary" type="submit">保存</button>
                    <span class="btn btn-white" onclick="history.go(-1)">返回</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
