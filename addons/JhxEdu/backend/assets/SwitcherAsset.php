<?php
namespace addons\JhxEdu\backend\assets;

class SwitcherAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@addons/JhxEdu/backend/static';

    public $js = [
        'plugins/switchery/switchery.min.js',
        'plugins/layer/layer.js'
    ];

    public $css = [
        'plugins/switchery/switchery.min.css'
    ];
}