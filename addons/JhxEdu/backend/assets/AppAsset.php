<?php

namespace addons\JhxEdu\backend\assets;

use yii\web\AssetBundle;

/**
 * 静态资源管理
 *
 * Class AppAsset
 * @package addons\JhxEdu\backend\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@addons/JhxEdu/backend/resources/';

    public $css = [
    ];

    public $js = [
    ];

    public $depends = [
    ];
}