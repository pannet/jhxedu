<?php
return [
    // ----------------------- 菜单配置 ----------------------- //
    'config' => [
        // 菜单配置
        'menu' => [
            'location' => 'default', // default:系统顶部菜单;addons:应用中心菜单
            'icon' => 'fa fa-puzzle-piece',
        ],
        // 子模块配置
        'modules' => [
            // 公共
            'common' => [
                'class' => 'addons\JhxEdu\backend\modules\common\Module',
            ],
            // 课程
            'product' => [
                'class' => 'addons\JhxEdu\merchant\modules\product\Module',
            ],
            // 订单
            'order' => [
                'class' => 'addons\JhxEdu\merchant\modules\order\Module',
            ],
            // 直播
            'live' => [
                'class' => 'addons\JhxEdu\backend\modules\live\Module',
            ],
            // 教师
            'teacher' => [
                'class' => 'addons\JhxEdu\merchant\modules\teacher\Module',
            ],
        ],
    ],

    // ----------------------- 快捷入口 ----------------------- //

    'cover' => [

    ],

    // ----------------------- 菜单配置 ----------------------- //

    'menu' => [
        [
            'title' => '广告管理',
            'route' => 'common/adv/index',
            'icon' => 'fa-share-alt'
        ],
        [
            'title' => '导航管理',
            'route' => 'common/nav/index',
            'icon' => 'fa fa-paper-plane'
        ],
        [
            'title' => 'vip设置',
            'route' => 'common/member-vip/index',
            'icon' => 'fa fa-diamond'
        ],
        [
            'title' => '课程分类',
            'route' => 'product/cate/index',
            'icon' => 'fa fa-folder'
        ],
        [
            'title' => '课程管理',
            'route' => 'product/product/index',
            'icon' => 'fa fa-book'
        ],
        [
            'title' => '订单管理',
            'route' => 'marketing',
            'icon' => 'fa fa-sticky-note',
            'child' => [
                [
                    'title' => '课程订单',
                    'route' => 'order/order/index?order_type=1',
                ],
                [
                     'title' => 'vip订单',
                    'route' => 'order/order/index?order_type=2',
                ],
            ]
        ],
        [
            'title' => '基础设置',
            'route' => 'setting/display',
            'icon' => 'fa fa fa-gear'
        ],
        [
            'title' => '小程序直播',
            'route' => 'live/live/index',
            'icon' => 'fa fa-book'
        ],
        [
            'title' => '教师管理',
            'route' => 'teacher/teacher/index',
            'icon' => 'fa fa-book'
        ],
    
    ],

    // ----------------------- 权限配置 ----------------------- //

    'authItem' => [
        [
            'title' => '所有权限',
            'name' => '*',
        ],
    ],
];