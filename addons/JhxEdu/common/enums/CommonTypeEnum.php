<?php

namespace addons\JhxEdu\common\enums;

use common\enums\BaseEnum;
use addons\JhxEdu\common\models\product\Product;

/**
 * 公用映射类
 *
 * 必须带有字段 collect_num, transmit_num, nice_num
 *
 * Class CommonTypeEnum
 * @package addons\JhxEdu\common\enums
 */
class CommonTypeEnum extends BaseEnum
{
    const PRODUCT = 'product';

    /**
     * @return array
     */
    public static function getMap(): array
    {
        return [
            self::PRODUCT => Product::class,
        ];
    }
}