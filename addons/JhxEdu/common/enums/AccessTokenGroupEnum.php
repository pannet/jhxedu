<?php

namespace addons\jhxEdu\common\enums;

use common\enums\BaseEnum;

/**
 * Class AccessTokenGroupEnum
 * @package addons\jhxEdu\common\enums
 */
class AccessTokenGroupEnum extends BaseEnum
{
    /**
     * 组别 主要用于多端登录
     */
    const DEFAULT = 'jhxEdu';
    const IOS = 'jhxEduIos'; // ios
    const ANDROID = 'jhxEduAndroid'; // 安卓
    const APP = 'jhxEduApp'; // app通用
    const H5 = 'jhxEduH5'; // H5
    const PC = 'jhxEduPc'; // Pc
    const WECHAT = 'jhxEduWechat'; // 微信公众号
    const WECHAT_MQ = 'jhxEduWechatMq'; // 微信小程序
    const ALI_MQ = 'jhxEduAliMq'; // 支付宝小程序
    const QQ_MQ = 'jhxEduQqMq'; // QQ小程序
    const BAIDU_MQ = 'jhxEduBaiduMq'; // 百度小程序
    const DING_TALK_MQ = 'jhxEduDingTalkMq'; // 钉钉小程序
    const TOU_TIAO_MQ = 'jhxEduTouTiaoMq'; // 头条小程序

    /**
     * @return array
     */
    public static function getMap(): array
    {
        return [
            self::DEFAULT => '默认',
            self::IOS => 'Ios',
            self::ANDROID => 'Android',
            self::APP => 'App',
            self::H5 => 'H5',
            self::PC => 'Pc',
            self::WECHAT => '微信',
            self::WECHAT_MQ => '微信小程序',
            self::ALI_MQ => '支付宝小程序',
            self::QQ_MQ => 'QQ小程序',
            self::BAIDU_MQ => '百度小程序',
            self::DING_TALK_MQ => '钉钉小程序',
            self::TOU_TIAO_MQ => '头条小程序',
        ];
    }
}