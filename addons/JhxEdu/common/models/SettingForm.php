<?php

namespace addons\JhxEdu\common\models;

use yii\base\Model;

/**
 * Class SettingForm
 * @package addons\JhxEdu\common\models
 */
class SettingForm extends Model
{
    public $share_title;
    public $share_cover;
    public $share_desc;
    public $share_link;
    //支付类型
    public $edu_pay_method;
    //账号类型
    public $edu_account_type;
    //是否开启第三方授权注册，类似授权登录
    public $register_auth = 1;
    //注册时否需要密码
    public $register_require_pass = 1;
    //开启分销
    public $edu_fenxiao;
    //分销类型
    public $edu_fenxiao_type = 1;
    //店铺设置
    public $shop_name;
    public $shop_address;
    public $shop_tel;
    public $shop_image;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'edu_pay_method',
                    'edu_account_type',
                    'edu_fenxiao',
                    'edu_fenxiao_type',
                    'register_auth',
                    'register_require_pass'
                ],
                'integer',
            ],
            [['share_title', 'share_cover','shop_tel'], 'string', 'max' => 100],
            [['share_link', 'share_desc','shop_name','shop_address','shop_image'], 'string', 'max' => 255],
            [['share_link'], 'url'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'edu_pay_method'=>'支付方式',
            'edu_account_type'=>'账号形式',
            'edu_fenxiao'=>'分销设置',
            'share_title' => '分享标题',
            'share_cover' => '分享封面',
            'share_desc' => '分享描述',
            'share_link' => '分享链接',
            'edu_fenxiao_type'=>'分销方式',
            'register_auth'=>'是否开启第三方授权注册',
            'register_require_pass'=>'注册时否需要密码',
            'shop_name'=>'店铺名称',
            'shop_address'=>'店铺地址',
            'shop_tel'=>'店铺电话',
            'shop_image'=>'店铺门头照片'
        ];
    }
}