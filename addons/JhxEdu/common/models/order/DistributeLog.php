<?php

namespace addons\JhxEdu\common\models\order;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\models\member\Member;
/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class DistributeLog extends \common\models\base\BaseModel
{
    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_distribute_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id','pid','order_id','money'], 'required'],
            [['pid','member_id','order_id'],'integer'],
            [['money'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    /**
     * 关联用户
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id'])->select(['id', 'nickname', 'head_portrait']);;
    }

}