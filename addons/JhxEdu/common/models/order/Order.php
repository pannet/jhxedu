<?php

namespace addons\JhxEdu\common\models\order;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\models\member\Member;
/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class Order extends \common\models\base\BaseModel
{
    public $page_size = 10;
     /**
     * 生成的订单产品
     *
     * @var array|OrderProduct
     */
    public $products_id = [];
    public $orderProducts;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id','pay_money','order_money','order_sn','order_type'], 'required'],
            [['pid'],'integer'],
            [['fenxiao_money'], 'number'],
            [['order_from'], 'string'],
            
        ];
    }
    
    /**
     * 场景
     *
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['member_id','pay_money','order_money','order_from','order_sn','buyer_ip','status','order_type','pid','fenxiao_money'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'products_id'=>'可购买课程',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    /**
     * 关联商品
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasMany(OrderProduct::class, ['order_id' => 'id']);
    }
    /**
     * 用户信息
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }
}