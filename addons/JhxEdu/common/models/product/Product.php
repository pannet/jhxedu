<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\behaviors\MerchantBehavior;
use common\enums\StatusEnum;
use common\traits\HasOneMerchant;
use addons\JhxEdu\common\enums\CommonTypeEnum;
use addons\JhxEdu\common\enums\OrderStatusEnum;
use addons\JhxEdu\common\models\common\Collect;
use addons\JhxEdu\common\models\order\OrderProduct;

/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class Product extends \common\models\base\BaseModel
{
    use MerchantBehavior, HasOneMerchant;

    const PRODUCT_STATUS_PUTAWAY = 1;
    const PRODUCT_STATUS_SOLD_OUT = 0;
    const PRODUCT_STATUS_FORBID = 10; // 禁售

    /**
     * 上下架
     *
     * @var array
     */
    public static $productStatusExplain = [
        self::PRODUCT_STATUS_PUTAWAY => '上架',
        self::PRODUCT_STATUS_SOLD_OUT => '下架',
    ];
    public $keyword;
    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'cate_id', 'real_price'], 'required'],
            [['status','virtual_sale','is_top','product_status','sort','show_type','price_type','type','content_type','pre_time'],'integer'],
            [['market_price','vip_price','fenxiao_money'], 'number'],
            [['cover'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '课程名称',
            'cate_id' => '分类',
            'cover'=> '封面',
            'intro' => '课程描述',
            'real_price'=>'售价',
            'market_price'=>'原价',
            'vip_price'=>'会员价格',
            'status' => '状态(勾选为启用)',
            'is_top'=>'是否推荐',
            'sale' => '销量',
            'virtual_sale' => '虚拟销量',
            'sort' => '排序',
            'fenxiao_money'=>'分销金额',
            'product_status' => '上架状态',
            'price_type' => '收费方式',
            'type'=>'类型',
            'content_type'=>'内容类型',
            'pre_time'=>'试看时长',
            'created_at' => '创建时间',
            'updated_at' => '最后更新时间',
        ];
    }


    /**
     * 关联详情
     *
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasOne(ProductData::class, ['product_id' => 'id'])
            ->select(['content','video','voice']);
    }

    
    /**
     * 关联分类
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCate()
    {
        return $this->hasOne(Cate::class, ['id' => 'cate_id'])
            ->select(['id', 'title']);
    }
    /**
     * 关联拼团
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPintuan()
    {
        return $this->hasOne(ProductPintuan::class, ['product_id' => 'id']);
    }

    /**
     * 关联评价
     *
     * @return ActiveQuery
     */
    public function getEvaluate()
    {
        return $this->hasMany(Evaluate::class, ['product_id' => 'id'])
            ->select([
                'id',
                'product_id',
                'sku_name',
                'content',
                'covers',
                'explain_first',
                'member_id',
                'member_nickname',
                'member_head_portrait',
                'is_anonymous',
                'scores',
                'again_content',
                'again_covers',
                'again_explain',
                'again_addtime',
                'explain_type',
                'created_at',
            ])
            ->limit(10)
            ->orderBy('id desc')
            ->asArray();
    }

    /**
     * 阶梯优惠
     *
     * @return ActiveQuery
     */
    public function getLadderPreferential()
    {
        return $this->hasMany(LadderPreferential::class,
            ['product_id' => 'id'])->orderBy('quantity desc, id asc')->asArray();
    }

    /**
     * 我的收藏
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMyCollect()
    {
        return $this->hasOne(Collect::class, ['topic_id' => 'id'])
            ->where(['topic_type' => CommonTypeEnum::PRODUCT])
            ->andWhere(['status' => StatusEnum::ENABLED]);
    }

    /**
     * 我的购买总数量
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMyGet()
    {
        return $this->hasOne(OrderProduct::class, ['product_id' => 'id'])
            ->select(['sum(num) as all_num', 'product_id'])
            ->where(['status' => StatusEnum::ENABLED])
            ->andWhere(['in', 'order_status', OrderStatusEnum::haveBought()])
            ->andWhere(['merchant_id' => Yii::$app->services->merchant->getId()])
            ->groupBy('product_id');
    }
}