<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class ProductData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['content'], 'string', 'max' => 10000],
            [['video','voice'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => '课程id',
            'content' => '课程详情',
            'video'=>'视频文件',
            'voice'=>'音频文件'
        ];
    }
}