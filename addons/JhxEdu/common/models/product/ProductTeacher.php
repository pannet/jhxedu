<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class ProductTeacher extends ActiveRecord
{
    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product_teacher}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id','teacher_id'], 'required'],
            [['product_id','teacher_id'],'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => '课程id',
            'teacher_id'=> '任课教师',
        ];
    }
     /**
     * 关联课程
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}