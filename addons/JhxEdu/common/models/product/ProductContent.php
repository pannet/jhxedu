<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\enums\StatusEnum;
use common\behaviors\MerchantBehavior;
use common\traits\HasOneMerchant;
use addons\JhxEdu\common\enums\CommonTypeEnum;
use addons\JhxEdu\common\enums\OrderStatusEnum;

/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class ProductContent extends \common\models\base\BaseModel
{

    const PRODUCT_STATUS_PUTAWAY = 1;
    const PRODUCT_STATUS_SOLD_OUT = 0;
    const PRODUCT_STATUS_FORBID = 10; // 禁售

    /**
     * 上下架
     *
     * @var array
     */
    public static $productStatusExplain = [
        self::PRODUCT_STATUS_PUTAWAY => '上架',
        self::PRODUCT_STATUS_SOLD_OUT => '下架',
    ];
    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product_content}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'product_id','price_type','type'], 'required'],
            [['type','price_type','sort','pre_time'],'integer'],
            [['src'], 'safe'],
            [['name','author','video','voice'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => '课程id',
            'name'=>'章节名称',
            'video'=>'视频文件',
            'voice'=>'音频文件',
            'price_type'=>'收费方式',
            'author'=>'上课老师',
            'type'=>'类型',
            'pre_time'=>'免费浏览时间（秒）',
            'status'=>'启用',
            'sort' => '排序',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    
    /**
     * 关联课程
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Cate::class, ['id' => 'product_id'])
            ->select(['id', 'name']);
    }

    /**
     * 关联详情
     *
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasOne(ProductContentData::class, ['content_id' => 'id'])
            ->select(['content_id', 'data']);
    }

    /**
     * 关联评价
     *
     * @return ActiveQuery
     */
    public function getEvaluate()
    {
        return $this->hasMany(Evaluate::class, ['product_id' => 'id'])
            ->select([
                'id',
                'product_id',
                'sku_name',
                'content',
                'covers',
                'explain_first',
                'member_id',
                'member_nickname',
                'member_head_portrait',
                'is_anonymous',
                'scores',
                'again_content',
                'again_covers',
                'again_explain',
                'again_addtime',
                'explain_type',
                'created_at',
            ])
            ->limit(10)
            ->orderBy('id desc')
            ->asArray();
    }
}