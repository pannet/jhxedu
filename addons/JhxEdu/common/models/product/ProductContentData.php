<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class ProductContentData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product_content_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id'], 'required'],
            [['data'], 'string', 'max' => 10000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => '章节id',
            'data' => '章节详情',
        ];
    }
}