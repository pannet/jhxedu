<?php

namespace addons\JhxEdu\common\models\product;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\behaviors\MerchantBehavior;
use common\traits\HasOneMerchant;
use yii\db\ActiveRecord;
use common\models\member\Member;
/**
 * This is the model class for table "{{%addon_edu_product}}".
 *
 */
class ProductStudent extends ActiveRecord
{

    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_product_student}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id','member_id','type'], 'required'],
            [['product_id','member_id','type'],'integer'],
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => '课程id',
            'member_id'=>'学员id',
            'type'=>'类型',
        ];
    }

    
    /**
     * 关联课程
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])
            ->select(['id', 'name']);
    }

   /**
     * 关联课程
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id'])
            ->select(['id', 'nickname','realname','mobile']);
    }

   
}