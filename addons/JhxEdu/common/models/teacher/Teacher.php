<?php

namespace addons\JhxEdu\common\models\teacher;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use common\behaviors\MerchantBehavior;
use common\enums\StatusEnum;
use common\traits\HasOneMerchant;
use addons\JhxEdu\common\enums\CommonTypeEnum;
use addons\JhxEdu\common\enums\OrderStatusEnum;
use addons\JhxEdu\common\models\common\Collect;

/**
 * This is the model class for table "{{%addon_shop_product}}".
 *
 */
class Teacher extends \common\models\base\BaseModel
{
    use MerchantBehavior, HasOneMerchant;

    const PRODUCT_STATUS_PUTAWAY = 1;
    const PRODUCT_STATUS_SOLD_OUT = 0;
    const PRODUCT_STATUS_FORBID = 10; // 禁售

    /**
     * 上下架
     *
     * @var array
     */
    public static $productStatusExplain = [
        self::PRODUCT_STATUS_PUTAWAY => '上架',
        self::PRODUCT_STATUS_SOLD_OUT => '下架',
    ];
    public $keyword;
    public $page_size = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_teacher}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status','sort'],'integer'],
            [['avatar'], 'safe'],
            [['name','school','level'], 'string', 'max' => 255],
            [['intro'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '教师姓名',
            'avatar'=> '教师头像',
            'school'=> '毕业学校',
            'intro'=> '简介',
            'sort' => '排序',
            'level' => '头衔',
            'status' => '状态',
            'created_at' => '注册时间',
            'updated_at' => '更新时间',
        ];
    }


    /**
     * 关联详情
     *
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasOne(TeacherData::class, ['teacher_id' => 'id'])
            ->select(['education','feature','detail']);
    }

    
    /**
     * 关联分类
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCate()
    {
        return $this->hasOne(Cate::class, ['id' => 'cate_id'])
            ->select(['id', 'title']);
    }
    public static function getALL(){
      return self::find()->select('name')->where(['status'=>1])->indexBy('id')->column();
    }
    /**
     * 我的收藏
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMyCollect()
    {
        return $this->hasOne(Collect::class, ['topic_id' => 'id'])
            ->where(['topic_type' => CommonTypeEnum::PRODUCT])
            ->andWhere(['status' => StatusEnum::ENABLED]);
    }
}