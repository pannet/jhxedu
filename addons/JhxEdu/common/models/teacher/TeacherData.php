<?php

namespace addons\JhxEdu\common\models\teacher;

use common\helpers\StringHelper;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%addon_edu_teacher_data}}".
 *
 */
class TeacherData extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_teacher_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id'], 'required'],
            [['education','feature'], 'string', 'max' => 1000],
            [['detail'], 'string', 'max' => 10000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'teacher_id' => '教师id',
            'education' => '教育经历',
            'feature' => '教学特点',
            'detail' => '教师详情',
        ];
    }
}