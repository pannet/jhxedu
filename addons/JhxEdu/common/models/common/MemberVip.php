<?php

namespace addons\JhxEdu\common\models\common;

use Yii;
use common\behaviors\MerchantBehavior;
use common\enums\StatusEnum;
use common\traits\HasOneMerchant;
/**
 * This is the model class for table "rf_addon_edu_member_vip".
 *
 * @property int $id
 * @property string $name 名称
 * @property int $day 有效时长(天)
 * @property string $price 售价
 * @property string $market_price 原价
 */
class MemberVip extends \common\models\base\BaseModel
{
    use MerchantBehavior, HasOneMerchant;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_member_vip}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'day', 'price', 'market_price'], 'required'],
            [['day'], 'integer'],
            [['price', 'market_price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'day' => '有效时长(天)',
            'price' => '售价',
            'market_price' => '原价',
            'status' => '启用',
        ];
    }
}
