<?php

namespace addons\JhxEdu\common\models\common;

use addons\JhxEdu\common\models\product\Product;
use common\traits\HasOneMerchant;

/**
 * Class Collect
 * @package addons\JhxEdu\common\models\common
 */
class Collect extends Follow
{
    use HasOneMerchant;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_common_collect}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'topic_id'])
            ->select(['id', 'name','real_price','vip_price', 'picture','collect_num', 'view', 'product_status', 'status']);
    }
}
