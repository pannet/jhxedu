<?php

namespace addons\JhxEdu\common\models\common;

use common\helpers\Util;
use Yii;

/**
 * This is the model class for table "{{%nav}}".
 *
 * @property integer $id
 * @property string $key
 */
class Nav extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%addon_edu_nav}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'title'], 'required'],
            [['key', 'title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
    }

    public function getActiveItem()
    {
        return $this->hasMany(NavItem::className(), ['nav_id' => 'id'])->where(['status' => 1]);
    }

    public static function getItems($key)
    {
        $nav = self::find()->where(['key' => $key])->one();
        if ($nav == null) {
            return [];
        }
        $items = NavItem::find()->select('title label, icon,url, target')
            ->where(['nav_id' => $nav->id, 'status' => 1])
            ->orderBy(['sort' => SORT_ASC])
            ->asArray()->all();
        return array_map(function($value){
            $value['url'] = Util::parseUrl($value['url']);
            if ($value['target'] == 1) {
                $value['linkOptions'] = ['target' => '_blank'];
            }
            return $value;
        }, $items);
    }

}
