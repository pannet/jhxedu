<?php

namespace addons\JhxEdu\common\models\common;

use addons\JhxEdu\common\behaviors\PositionBehavior;
use Yii;

/**
 * This is the model class for table "{{%nav_item}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $status
 */
class NavItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%addon_edu_nav_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['status', 'nav_id', 'sort', 'target'], 'integer'],
            [['title', 'url'], 'string', 'max' => 128],
            ['icon','string'],	
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'nav_id' => '类型id',
            'title' => '标题',
            'url' => 'h5链接',
            'target' => '是否新窗口打开',
            'status' => '状态',
            'sort' => '排序',
            'icon' => '图标',
        ];
    }

    public function attributeHints()
    {
        return [
            'url' => '带http的绝对地址',
            'icon'=>'图标尺寸为：80*80'
        ];
    }
    public function getNav()
    {
        return $this->hasOne(Nav::className(), ['id' => 'nav_id']);
    }
}
