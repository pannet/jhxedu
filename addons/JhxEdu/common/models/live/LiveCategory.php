<?php

namespace addons\JhxEdu\common\models\live;

use common\behaviors\PositionBehavior;
use common\behaviors\CacheInvalidateBehavior;
use common\behaviors\MetaBehavior;
use common\helpers\Tree;
use common\models\behaviors\CategoryBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\Expression;
use common\models\Article;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "{{%article}}".
 *
 * @property int $id
 * @property int $pid
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property string $module
 * @property string $cover
 * @property int $allow_publish
 */
class LiveCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%addon_edu_live_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            //中文没法自动生成slug，又没必要必填
            ['slug', 'default', 'value' => function ($model) {
                return $model->title;
            }],
            ['module', 'safe'],
            [['pid', 'sort', 'allow_publish','width','height'], 'integer'],
            ['pid', 'default', 'value' => 0],
            [['sort'], 'default', 'value' => 0]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '分类名',
            'slug' => '标识',
            'pid' => '上级分类',
            'ptitle' => '上级分类', // 非表字段,方便后台显示
            'description' => '分类介绍',
            'article' => '直播数', //冗余字段,方便查询
            'sort' => '排序',
			'width' => '',
			'height' => '',
            'module' => '',
            'allow_publish' => '',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => MetaBehavior::className(),
            ],
            CategoryBehavior::className(),
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
                'positionAttribute' => 'sort',
                'groupAttributes' => [
                    'pid'
                ],
            ],
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags' => [
                    'categoryList'
                ]

            ]
        ];
    }

    public function getMetaData()
    {
        $model =  $this->getMetaModel();
        $title = $model->title ? : $this->title;
        $keywords = $model->keywords;
        $description =$model->description ? : $this->description;

        return [$title, $keywords, $description];
    }

    /**
     * 获取分类名
     */
    public function getPtitle($cid)
    {
        return static::find()->select('title')->where(['id' => $cid])->all();
    }

    public static function lists()
    {
        $list = Yii::$app->cache->get(['liveCategoryList']);
        if ($list === false) {
            $query = static::find();
            $list = $query->asArray()->all();
            Yii::$app->cache->set(['liveCategoryList'], $list, 0, new TagDependency(['tags' => ['liveCategoryList']]));
        }
        return $list;
    }

    public static function getDropDownList($tree = [], &$result = [], $deep = 0, $separator = '--')
    {
        $deep++;
        foreach($tree as $list) {
            $result[$list['id']] = str_repeat($separator, $deep-1) . $list['title'];
            if (isset($list['children'])) {
                self::getDropDownList($list['children'], $result, $deep);
            }
        }
        return $result;
    }

    public function getCategoryNameById($id)
    {
        $list = $this->lists();
        return isset($list[$id]) ? $list[$id] : null;
    }

    public static function getIdByName($name)
    {
        $list = self::lists();

        return array_search($name, $list);
    }

    public static function findByIdOrSlug($id)
    {
        if (intval($id) == 0) {
            $condition = ["slug" => $id];
        } else {
            $condition = [
                $id
            ];
        }

        return static::findOne($condition);
    }

    public static function getAllowPublishEnum()
    {
        return [
            '不允许',
            '只允许后台',
            '允许前后台'
        ];
    }


	/*通过父级ID获取全部的子ID*/
	public function getChilds($pid){
		$childs=(new \yii\db\Query())->select("id")->from(self::tableName())->where(['pid'=> $pid])->all();
        return $childs;
	}

}
