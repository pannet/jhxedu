<?php
namespace addons\JhxEdu\common\models\live;
use yii\behaviors\TimestampBehavior;
use common\modules\attachment\behaviors\UploadBehavior;
use common\behaviors\MerchantBehavior;
use common\helpers\StringHelper;
use Yii;
/**
 * This is the model class for table "{{%live}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $roomid
 * @property string $cover_img
 * @property integer $live_status
 * @property integer $start_time
 * @property integer $end_time
 * @property string $anchor_name
 * @property string $anchor_img
 * @property string $goods
 * @property string $live_replay
 * @property integer $is_top
 * @property integer $is_del
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sort
 */
class Live extends \yii\db\ActiveRecord
{
    public $cover_img;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%addon_edu_live}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','cate_id'],'required'],
            ['name', 'string', 'min' => 3, 'max' => 16,"tooLong"=>"不能超过16个汉字", "tooShort"=>"标题至少是3个汉字"],
            [['id','cate_id','close_goods','close_comment','screen_type','close_like','roomid', 'live_status', 'start_time', 'end_time', 'is_top', 'is_del', 'sort'], 'integer'],
            [['goods', 'live_replay','type'], 'string'],
            ['anchor_name', 'string', 'min' => 2, 'max' => 15,"tooLong"=>"不能超过15个汉字", "tooShort"=>"主播名字至少是2个汉字"],
            [['anchor_wechat'], 'string', 'max' => 145],
            [['anchor_img','share_img','cover_img','url'], 'string', 'max' => 512],
            ['sort','default','value'=>99],
            [['roomid'], 'unique'],
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => '标题',
            'roomid' => '直播间id',
            'share_img'=>'分享图',
            'cover_img' => '直播封面',
            'live_status' => '直播状态',// 101: 直播中, 102: 未开始, 103: 已结束, 104: 禁播, 105: 暂停中, 106: 异常, 107: 已过期
            'start_time' => '直播计划开始时间',
            'end_time' => '直播计划结束时间',
            'anchor_name' => '主播名',
            'anchor_img' => '主播头像',
            'goods' => '商品',
            'live_replay' => '回放内容',
            'is_top' => '置顶',
            'is_del' => '状态',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sort' => '排序',
            'url' => '视频地址',
            'type' => '类型',
            'close_goods' => '直播货物',
            'close_comment' => '直播评论',
            'screen_type' => '横竖屏',
            'anchor_wechat' => '主播微信',
            'close_like' => '直播点赞',
            'cate_id' => '分类',
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = [
            TimestampBehavior::className(),
        
        ];
        return $behaviors;
    }
    public function get_status($live_status){
        switch ($live_status){
            case 101:$str='直播中';break;
            case 102:$str='未开始';break;
            case 103:$str='已结束';break;
            case 104:$str='禁播';break;
            case 105:$str='暂停中';break;
            case 106:$str='异常';break;
            case 107:$str='已过期';break;
            default:$str='异常';break;
        }
        return $str;
    }
    
    public function getCate()
    {
        return $this->hasOne(LiveCategory::class, ['id' => 'cate_id']);
    }
    public function upload()
    {
        if ($this->cover_img&&$this->share_img) {
            $time1 = time();
            $time2 = time()+1;
            $this->cover_img->saveAs('../storage/weixin/' . $time1 . '.' . $this->cover_img->extension);
            $this->share_img->saveAs('../storage/weixin/' . $time2 . '.' . $this->share_img->extension);
            return [
                'cover'=>$time1.'.' . $this->cover_img->extension,
                'share'=>$time2.'.'. $this->share_img->extension];
        } else {
            return false;
        }
    }
    
}
