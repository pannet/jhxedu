<?php

namespace addons\JhxEdu\common\models\forms;

use yii\base\Model;

/**
 * Class OrderQueryForm
 * @package addons\JhxEdu\common\models\forms
 * @author jianyan74 <751393839@qq.com>
 */
class OrderQueryForm extends Model
{
    public $order_type = '';
    public $start_time = '';
    public $end_time = '';
    public $order_sn;
    public $member_id;
    public $pay_status;
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_type', 'order_sn'], 'string'],
            [['member_id','pay_status'], 'integer'],
        ];
    }
}