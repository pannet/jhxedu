<?php

namespace addons\JhxEdu\common\models\forms;

use Yii;
use yii\base\Model;
use common\interfaces\PayHandler;

/**
 * Class OrderPayFrom
 * @package addons\JhxEdu\common\models\forms
 */
class OrderPayFrom extends Model implements PayHandler
{
    /**
     * @var
     */
    public $order_id;

    protected $order;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['order_id', 'required'],
            ['order_id', 'integer', 'min' => 0],
            ['order_id', 'verifyPay'],
        ];
    }

    /**
     * @param $attribute
     */
    public function verifyPay($attribute)
    {
        $this->order = Yii::$app->jhxEduService->order->findById($this->order_id);
        if (!$this->order) {
            $this->addError($attribute, '找不到订单');

            return;
        }

        if ($this->order['order_status'] != 1) {
            $this->addError($attribute, '订单处于不可支付状态');

            return;
        }
    }

    /**
     * 支付说明
     *
     * @return string
     */
    public function getBody(): string
    {
        return '订单支付';
    }

    /**
     * 支付详情
     *
     * @return string
     */
    public function getDetails(): string
    {
        return '';
    }

    /**
     * 支付金额
     *
     * @return float
     */
    public function getTotalFee(): float
    {
        return $this->order['pay_money'];
    }

    /**
     * 获取订单号
     *
     * @return float
     */
    public function getOrderSn(): string
    {
        return $this->order['order_sn'];
    }

    /**
     * 是否查询订单号(避免重复生成)
     *
     * @return bool
     */
    public function isQueryOrderSn(): bool
    {
        return true;
    }
}