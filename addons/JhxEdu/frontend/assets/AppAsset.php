<?php

namespace addons\JhxEdu\frontend\assets;

use yii\web\AssetBundle;

/**
 * 静态资源管理
 *
 * Class AppAsset
 * @package addons\JhxEdu\frontend\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@addons/JhxEdu/frontend/resources/';

    public $css = [
    ];

    public $js = [
    ];

    public $depends = [
    ];
}