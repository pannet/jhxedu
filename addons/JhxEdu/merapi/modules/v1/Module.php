<?php

namespace addons\JhxEdu\merapi\modules\v1;

/**
 * Class Module
 * @package addons\JhxEdu\merapi\modules\v1 * @author jianyan74 <751393839@qq.com>
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\merapi\modules\v1\controllers';

    public function init()
    {
        parent::init();
    }
}