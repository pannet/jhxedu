<?php

namespace addons\JhxEdu\merchant\assets;

use yii\web\AssetBundle;

/**
 * 静态资源管理
 *
 * Class AppAsset
 * @package addons\JhxEdu\merchant\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@addons/JhxEdu/merchant/resources/';

    public $css = [
    ];

    public $js = [
    ];

    public $depends = [
    ];
}