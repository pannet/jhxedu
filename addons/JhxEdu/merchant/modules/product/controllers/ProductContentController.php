<?php

namespace addons\JhxEdu\merchant\modules\product\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use common\enums\StatusEnum;
use common\models\base\SearchModel;
use common\helpers\ResultHelper;
use addons\JhxEdu\common\models\product\Product;
use addons\JhxEdu\common\models\product\ProductContent;
use addons\JhxEdu\common\models\product\ProductContentData;
use addons\JhxEdu\merchant\controllers\BaseController;
use common\traits\MerchantCurd;
use Overtrue\Flysystem\Qiniu\QiniuAdapter;
/**
 * Class ProductController
 * @package addons\JhxEdu\merchant\controllers
 */
class ProductContentController extends BaseController
{
    use MerchantCurd;
     /**
     * @var Product
     */
    public $modelClass = ProductContent::class;
    /**
     * 首页
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {

        $product_id = Yii::$app->request->get('product_id');
        $searchModel = new SearchModel([
            'model' => ProductContent::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::ENABLED])
            ->andWhere(['product_id' => $product_id]);

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'product_id' => $product_id,
        ]);
    }

    /**
     * 编辑/创建
     *
     * @return mixed
     */
    public function actionEdit()
    {
        $product_id = Yii::$app->request->get('product_id');
        $id = Yii::$app->request->get('id');
        $model = $this->findModel($id);
        //关联详情
        $modelData = $model->data;
        if(!$modelData){
            $modelData = new ProductContentData();
        }
        //
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $modelData->load(Yii::$app->request->post());
                $modelData->content_id = $model->id;
                $modelData->save();
                 //是否要裁剪视频
                if($model->video){
                    $backend_config = Yii::$app->debris->backendConfigAll();
                    $accessKey = $backend_config['storage_qiniu_accesskey'];
                    $secretKey = $backend_config['storage_qiniu_secrectkey'];
                    $cdnHost = $backend_config['storage_qiniu_domain'];
                    $bucket = $backend_config['storage_qiniu_bucket'];
                    $adapter = new QiniuAdapter($accessKey, $secretKey, $bucket, $cdnHost);
                    $config =  Yii::$app->params['uploadConfig']['videos'];
                    if($config['drive'] == 'qiniu' && $model->pre_time > 0){
                        $names = explode("/videos",$model->video);
                        $path = "videos".$names[1];
                        $adapter->avThumb($path,$config['solt'],$model->pre_time);
                    }
                }
                Yii::$app->getSession()->setFlash('success', '操作成功');
                return $this->redirect(['product-content/index','product_id'=>$model->product_id]);
            }
        }
        $model->loadDefaultValues();
        $product_id = $product_id ? $product_id : $model->product_id;
        $product = Product::find()->where(['id'=>$product_id])->one();
        $model->product_id = $product_id;
        return $this->render('edit', [
            'model' => $model,
            'modelData' => $modelData,
            'product' => $product,
        ]);
    }

    /**
     * 批量上下架
     *
     * @param $id
     * @return mixed
     */
    public function actionStateAll($state)
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        $product_status = $state == StatusEnum::ENABLED ? Product::PRODUCT_STATUS_PUTAWAY : Product::PRODUCT_STATUS_SOLD_OUT;

        Product::updateAll(['product_status' => $product_status], ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 删除 - 回收站
     *
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = StatusEnum::DISABLED;
        if ($model->save()) {
            return $this->message("删除成功", $this->redirect(['index','product_id'=>$model->product_id]));
        }

        return $this->message("删除失败", $this->redirect(['index','product_id'=>$model->product_id]), 'error');
    }

    /**
     * 批量删除 - 回收站
     *
     * @param $id
     * @return mixed
     */
    public function actionDeleteAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::DISABLED],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 伪删除 - 隐藏
     *
     * @param $id
     * @return mixed
     */
    public function actionDestroy($id)
    {
        if (!($model = $this->findModel($id))) {
            return $this->message("找不到数据", $this->redirect(['recycle']), 'error');
        }

        $model->status = StatusEnum::DELETE;
        if ($model->save()) {
            return $this->message("删除成功", $this->redirect(['recycle']));
        }

        return $this->message("删除失败", $this->redirect(['recycle']), 'error');
    }

    /**
     * 批量伪删除 - 隐藏
     *
     * @param $id
     * @return mixed
     */
    public function actionDestroyAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::DELETE],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 获取规格属性
     *
     * @param $model_id
     * @return array
     */
    public function actionBaseSpecAttribute($base_attribute_id)
    {
        // 属性值
        $data = Yii::$app->tinyShopService->baseAttribute->getDataById($base_attribute_id);
        $value = $data['value'] ?? [];
        foreach ($value as &$item) {
            $item['config'] = !empty($item['value']) ? explode(',', $item['value']) : [];
            $item['value'] = '';
        }

        // 规格
        $spec_ids = explode(',', $data['spec_ids']);
        $spec_ids = $spec_ids ?? [];
        $spec = Yii::$app->tinyShopService->baseSpec->getListWithValueByIds($spec_ids);

        return ResultHelper::json(200, '获取成功', [
            'attributeValue' => $value,
            'spec' => $spec ?? [],
        ]);
    }

    /**
     * 还原
     *
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->status = StatusEnum::ENABLED;
        if ($model->save()) {
            return $this->message("还原成功", $this->redirect(['recycle']));
        }

        return $this->message("还原失败", $this->redirect(['recycle']), 'error');
    }

    /**
     * 批量还原
     *
     * @param $id
     * @return mixed
     */
    public function actionRestoreAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::ENABLED],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 回收站
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRecycle()
    {
        $searchModel = new SearchModel([
            'model' => Product::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::DISABLED])
            ->with(['cate']);

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'cates' => Yii::$app->tinyShopService->productCate->getMapList(),
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSelect()
    {
        $this->layout = '@backend/views/layouts/default';
        $multiple = Yii::$app->request->get('multiple');
        $is_virtual = Yii::$app->request->get('is_virtual');
        $is_virtual == StatusEnum::ENABLED && $is_virtual = '';

        $searchModel = new SearchModel([
            'model' => ProductForm::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::ENABLED])
            ->andWhere(['product_status' => StatusEnum::ENABLED])
            ->andFilterWhere(['is_virtual' => $is_virtual])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->with(['cate']);

        /** @var  $gridSelectType */
        $gridSelectType = [
            'class' => 'yii\grid\CheckboxColumn',
            'property' => 'checkboxOptions',
        ];

        if ($multiple == false) {
            $gridSelectType = [
                'class' => 'yii\grid\RadioButtonColumn',
                'property' => 'radioOptions',
            ];
        }

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'gridSelectType' => $gridSelectType,
            'cates' => Yii::$app->tinyShopService->productCate->getMapList(),
        ]);
    }

    /**
     * 选择颜色
     *
     * @param $value
     * @return string
     */
    public function actionSelectColor()
    {
        $this->layout = '@backend/views/layouts/default';

        $value = Yii::$app->request->get('value');
        !$value && $value = '000000';
        $value = '#' . $value;

        return $this->render($this->action->id, [
            'value' => $value,
        ]);
    }

    /**
     * 创建商品页面
     *
     * @return string
     */
    public function actionCreate()
    {
        return $this->renderAjax($this->action->id, []);
    }

    /**
     * 返回模型
     *
     * @param $id
     * @return ProductForm|array|\yii\db\ActiveRecord|null
     */
    protected function findModel($id)
    {
        if (empty($id) || empty(($model = ProductContent::find()->where(['id' => $id])->one()))) {
            $model = new ProductContent();
            $model->loadDefaultValues();
        }

        return $model;
    }
}