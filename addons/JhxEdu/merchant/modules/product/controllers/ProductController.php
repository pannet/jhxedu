<?php

namespace addons\JhxEdu\merchant\modules\product\controllers;

use addons\JhxEdu\common\enums\DecimalReservationEnum;
use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use common\enums\StatusEnum;
use common\models\base\SearchModel;
use common\helpers\ResultHelper;
use addons\JhxEdu\common\models\product\Product;
use addons\JhxEdu\common\models\product\ProductData;
use addons\JhxEdu\common\models\product\ProductTeacher;
use addons\JhxEdu\common\models\product\ProductPintuan;
use addons\JhxEdu\common\models\product\ProductStudent;
use addons\JhxEdu\merchant\controllers\BaseController;
use common\traits\MerchantCurd;
/**
 * Class ProductController
 * @package addons\JhxEdu\merchant\controllers
 */
class ProductController extends BaseController
{
    use MerchantCurd;

    /**
     * @var Product
     */
    public $modelClass = Product::class;

    public function actions()
    {
        return [
                'switcher' => [
                        'class' => 'addons\JhxEdu\backend\widgets\grid\SwitcherAction'
                ]
        ];
    }
    /**
     * 首页
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => Product::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::ENABLED])
            ->with(['cate']);

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'cates' => Yii::$app->jhxEduService->productCate->getMapList(),
        ]);
    }

    /**
     * 编辑/创建
     *
     * @return mixed
     */
    public function actionEdit()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        $model = $this->findModel($id);
        $productTeacher = $this->teacherModel($id);
        $modelData = '';
        if($model->id){
            $modelData = ProductData::find()->where(['product_id'=>$model->id])->one();
        }

        if(!$modelData){
            $modelData = new ProductData();
        }
        //
        if($model->cover && $model->cover!= 'null'){
            $model->cover = unserialize($model->cover);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->picture = $model->cover ? $model->cover[0] : '';
            !empty($model->cover) && $model->cover = serialize($model->cover);
            if ($model->save()) {
                $modelData->load(Yii::$app->request->post());
                $modelData->product_id = $model->id;
                $modelData->save();
                $productTeacher->load(Yii::$app->request->post());
                $productTeacher->product_id = $model->id;
                $productTeacher->save();
                Yii::$app->getSession()->setFlash('success', '添加成功');
                return $this->redirect(['product/index']);
            }
        }
        //var_dump($modelData);die;
        return $this->render('edit', [
            'model' => $model,
            'productTeacher'=>$productTeacher,
            'modelData' => $modelData,
            'cates' => Yii::$app->jhxEduService->productCate->getMapList(),
        ]);
    }

    /**
     * 批量上下架
     *
     * @param $id
     * @return mixed
     */
    public function actionStateAll($state)
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        $product_status = $state == StatusEnum::ENABLED ? Product::PRODUCT_STATUS_PUTAWAY : Product::PRODUCT_STATUS_SOLD_OUT;

        Product::updateAll(['product_status' => $product_status], ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 删除 - 回收站
     *
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = StatusEnum::DISABLED;
        if ($model->save()) {
            return $this->message("删除成功", $this->redirect(['index']));
        }

        return $this->message("删除失败", $this->redirect(['index']), 'error');
    }

    /**
     * 批量删除 - 回收站
     *
     * @param $id
     * @return mixed
     */
    public function actionDeleteAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::DISABLED],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 拼团设置
     *
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAjaxPintuan()
    {
        $id = Yii::$app->request->get('id');
        $model = $this->pintuanModel($id);
        // ajax 校验
        $this->activeFormValidate($model);
        if ($model->load(Yii::$app->request->post())) {
            return $model->save()
                ? $this->redirect(['index'])
                : $this->message($this->getError($model), $this->redirect(['index']), 'error');
        }
        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }

    /**
     * 伪删除 - 隐藏
     *
     * @param $id
     * @return mixed
     */
    public function actionDestroy($id)
    {
        if (!($model = $this->findModel($id))) {
            return $this->message("找不到数据", $this->redirect(['recycle']), 'error');
        }

        $model->status = StatusEnum::DELETE;
        if ($model->save()) {
            return $this->message("删除成功", $this->redirect(['recycle']));
        }

        return $this->message("删除失败", $this->redirect(['recycle']), 'error');
    }

    /**
     * 批量伪删除 - 隐藏
     *
     * @param $id
     * @return mixed
     */
    public function actionDestroyAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::DELETE],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 还原
     *
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->status = StatusEnum::ENABLED;
        if ($model->save()) {
            return $this->message("还原成功", $this->redirect(['recycle']));
        }

        return $this->message("还原失败", $this->redirect(['recycle']), 'error');
    }

    /**
     * 批量还原
     *
     * @param $id
     * @return mixed
     */
    public function actionRestoreAll()
    {
        $ids = Yii::$app->request->post('ids', []);
        if (empty($ids)) {
            return ResultHelper::json(422, '请选择数据进行操作');
        }

        Product::updateAll(['status' => StatusEnum::ENABLED],
            ['and', ['in', 'id', $ids], ['merchant_id' => $this->getMerchantId()]]);

        return ResultHelper::json(200, '批量操作成功');
    }

    /**
     * 回收站
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRecycle()
    {
        $searchModel = new SearchModel([
            'model' => Product::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::DISABLED])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->with(['cate']);

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'cates' => Yii::$app->tinyShopService->productCate->getMapList(),
        ]);
    }

    /**
     * 更新排序/状态字段
     *
     * @param $id
     * @return array
     */
    public function actionAjaxUpdate($id)
    {
        if (!($model = Product::findOne($id))) {
            return ResultHelper::json(404, '找不到数据');
        }

        $model->attributes = ArrayHelper::filter(Yii::$app->request->get(), ['sort']);

        if (!$model->save()) {
            return ResultHelper::json(422, $this->getError($model));
        }

        return ResultHelper::json(200, '修改成功');
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSelect()
    {
        $this->layout = '@backend/views/layouts/default';
        $multiple = Yii::$app->request->get('multiple');
        $is_virtual = Yii::$app->request->get('is_virtual');
        $is_virtual == StatusEnum::ENABLED && $is_virtual = '';

        $searchModel = new SearchModel([
            'model' => ProductForm::class,
            'scenario' => 'default',
            'partialMatchAttributes' => ['name'], // 模糊查询
            'defaultOrder' => [
                'sort' => SORT_ASC,
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['status' => StatusEnum::ENABLED])
            ->andWhere(['product_status' => StatusEnum::ENABLED])
            ->andFilterWhere(['is_virtual' => $is_virtual])
            ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
            ->with(['cate']);

        /** @var  $gridSelectType */
        $gridSelectType = [
            'class' => 'yii\grid\CheckboxColumn',
            'property' => 'checkboxOptions',
        ];

        if ($multiple == false) {
            $gridSelectType = [
                'class' => 'yii\grid\RadioButtonColumn',
                'property' => 'radioOptions',
            ];
        }

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'gridSelectType' => $gridSelectType,
            'cates' => Yii::$app->tinyShopService->productCate->getMapList(),
        ]);
    }

    /**
     * 学员主页
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionStudent()
    {

        $product_id = Yii::$app->request->get('product_id');
        $searchModel = new SearchModel([
            'model' => ProductStudent::class,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
            'pageSize' => $this->pageSize,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->andWhere(['product_id' => $product_id]);

        return $this->render($this->action->id, [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'product_id' => $product_id
        ]);
    }

    /**
     * 学员设置
     *
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAjaxStudent()
    {
        $id = Yii::$app->request->get('id');
        $model = new ProductStudent();
        $model->loadDefaultValues();
        $model->product_id = $id;
        // ajax 校验
        $this->activeFormValidate($model);
        if ($model->load(Yii::$app->request->post())) {
            return $model->save()
                ? $this->redirect(['student','product_id'=>$id])
                : $this->message($this->getError($model), $this->redirect(['student','product_id'=>$id]), 'error');
        }
        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }
    /**
     * 创建商品页面
     *
     * @return string
     */
    public function actionCreate()
    {
        return $this->renderAjax($this->action->id, []);
    }

    /**
     * 返回模型
     *
     * @param $id
     * @return ProductForm|array|\yii\db\ActiveRecord|null
     */
    public function findModel($id)
    {
        if (empty($id) || empty(($model = Product::find()->where(['id' => $id])->andWhere(['merchant_id' => $this->getMerchantId()])->one()))) {
            $model = new Product();
            $model->merchant_id = $this->getMerchantId();
            $model->loadDefaultValues();
        }

        return $model;
    }

     /**
     * 返回模型
     *
     * @param $id
     * @return ProductForm|array|\yii\db\ActiveRecord|null
     */
    public function teacherModel($id)
    {
        if (empty($id) || empty(($model = ProductTeacher::find()->where(['product_id' => $id])->one()))) {
            $model = new ProductTeacher();
            $model->loadDefaultValues();
        }
        return $model;
    }
    /**
     * 返回模型
     *
     * @param $id
     * @return ProductForm|array|\yii\db\ActiveRecord|null
     */
    public function pintuanModel($id)
    {
        if (empty($id) || empty(($model = ProductPintuan::find()->where(['product_id' => $id])->one()))) {
            $model = new ProductPintuan();
            $model->loadDefaultValues();
        }
        return $model;
    }
    /**
     * 返回模型
     *
     * @param $id
     * @return ProductForm|array|\yii\db\ActiveRecord|null
     */
    protected function findFormModel($id)
    {
        if (empty($id) || empty(($model = ProductForm::find()->where(['id' => $id])->andFilterWhere(['merchant_id' => $this->getMerchantId()])->one()))) {
            $model = new ProductForm();
            $model->merchant_id = $this->getMerchantId();
            $model->loadDefaultValues();
        }

        return $model;
    }
}