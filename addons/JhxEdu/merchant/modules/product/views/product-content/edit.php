<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\webuploader\Files;
/* @var $this yii\web\View */
/* @var $model common\models\NavItem */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">基本信息</h3>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="col-lg-12">
                    <div>所属课程【<?= $product->name; ?>】</div>
                    <?= $form->field($model, 'product_id')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'author') ?>
                    <?= $form->field($model, 'price_type')->radioList([1=>'免费',2=>'vip免费',3=>'购买专栏后免费']) ?>
                    <?= $form->field($model, 'type')->radioList([1=>'图文',2=>'音频',3=>'视频']) ?>
                    <?= $form->field($model, 'voice',[
                       'options'=>['class' => 'voice content-type'], 
                    ])->widget(Files::class, [
                        'type' => 'voices',
                        'config' => [
                            // 可设置自己的上传地址, 不设置则默认地址
                            // 'server' => '',
                            'pick' => [
                                'multiple' => false,
                            ],
                            'accept' => [
                                'extensions' => ['mp3'],
                                'mimeTypes' => 'audio/*',
                            ],
                        ]
                    ])->hint(""); ?>
                    <?= $form->field($model, 'video',[
                       'options'=>['class' => 'video content-type'], 
                    ])->widget(Files::class, [
                        'type' => 'videos',
                        'config' => [
                            // 可设置自己的上传地址, 不设置则默认地址
                            // 'server' => '',
                            'pick' => [
                                'multiple' => false,
                            ],
                            'accept' => [
                                'extensions' => ['mpg', 'mpeg', 'mp4'],
                                'mimeTypes' => 'video/*',
                            ],
                        ]
                    ])->hint(""); ?>
                    <?= $form->field($model, 'pre_time')->hint("仅视频且使用七牛云存储时有效"); ?>
                    <?= $form->field($modelData, 'data')->widget(\common\widgets\ueditor\UEditor::class) ?>
                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' :  '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<style type="text/css">
    #productcontent-type{display: flex;}
    #productcontent-type .radio{margin-right: 30px;margin: 10px 30px 10px 0;}
    #productcontent-price_type{display: flex;}
    #productcontent-price_type .radio{margin-right: 30px;margin: 10px 30px 10px 0;}
</style>
