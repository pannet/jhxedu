<?php

use yii\grid\GridView;
use common\helpers\Html;
use common\helpers\ImageHelper;
use common\helpers\Url;
use common\enums\StatusEnum;
use addons\TinyShop\common\enums\VirtualProductGroupEnum;
use addons\TinyShop\common\enums\ProductShippingTypeEnum;

$this->title = '学员管理';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            学员管理
            <ul class="nav nav-tabs">
                <li><a href="<?= Url::to(['recycle']) ?>">回收站</a></li>
                <li class="pull-right">
                    <?= Html::a('添加学员', ['ajax-student', 'id' => $product_id], [
                        'data-toggle' => 'modal',
                        'data-target' => '#ajaxModal',
                        'class' => 'btn btn-white btn-sm'
                    ]) . '<br>'; ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="col-sm-12 m-b-sm">
                    <?= Html::a('批量删除</a>', "javascript:void(0);",
                        ['class' => 'btn btn-white btn-sm m-l-n-md delete-all']); ?>
                </div>
                <div class="active tab-pane">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        //重新定义分页样式
                        'tableOptions' => [
                            'class' => 'table table-hover rf-table',
                            'fixedNumber' => 3,
                            'fixedRightNumber' => 1,
                        ],
                        'options' => [
                            'id' => 'grid',
                        ],
                        'columns' => [
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                                'checkboxOptions' => function ($model, $key, $index, $column) {
                                    return ['value' => $model->id];
                                },
                            ],
                           
                            [
                                'attribute' => 'product_id',
                                'label' => '课程名称',
                                'value'=>function ($model, $key, $index, $column) {
                                    return $model->product->name;
                                },
                            ],
                            [
                                'attribute' => 'member_id',
                                'label' => '学员昵称',
                                'value'=>function ($model, $key, $index, $column) {
                                    return $model->member->nickname;
                                },
                            ],
                            [
                                'attribute' => 'mobile',
                                'label' => '学员手机',
                                'value'=>function ($model, $key, $index, $column) {
                                    return $model->member->mobile;
                                },
                            ],
                            [
                                'header' => "操作",
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}',
                                'buttons' => [
                                    'delete' => function ($url, $model, $key) {
                                        return Html::delete(['deleteStudent', 'id' => $model->id]);
                                    }
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
</div>

<script>
    let url = '';
    // 删除全部
    $(".delete-all").on("click", function () {
        url = "<?= Url::to(['delete-all'])?>";
        sendData(url);
    });

    // 上架
    $(".putaway-all").on("click", function () {
        url = "<?= Url::to(['state-all', 'state' => true])?>";
        sendData(url);
    });

    // 下架
    $(".sold-out-all").on("click", function () {
        url = "<?= Url::to(['state-all', 'state' => false])?>";
        sendData(url);
    });

    function sendData(url) {
        var ids = $("#grid").yiiGridView("getSelectedRows");
        $.ajax({
            type: "post",
            url: url,
            dataType: "json",
            data: {ids: ids},
            success: function (data) {
                if (parseInt(data.code) === 200) {
                    swal("操作成功", {
                        buttons: {
                            defeat: '确定',
                        },
                    }).then((value) => {
                        location.reload();
                    });
                } else {
                    rfWarning(data.message);
                }
            }
        });
    }
</script>