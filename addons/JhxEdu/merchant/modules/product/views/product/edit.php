<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\webuploader\Files;
use kartik\select2\Select2;
use addons\JhxEdu\common\models\teacher\Teacher;
/* @var $this yii\web\View */
/* @var $model common\models\NavItem */
/* @var $form yii\bootstrap\ActiveForm */
?>
<style type="text/css">
.field-product-show_type{display:flex;align-items: center;}
#product-show_type{display: flex;align-items: center;}
#product-show_type .radio{margin: 0;padding: 0;margin-left: 10px;}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">基本信息</h3>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($model, 'cate_id')->dropDownList($cates, [
                                'prompt' => '请选择',
                            ]) ?>
                        </div>
                    </div>
                    <?= $form->field($model, 'content_type')->radioList([1=>'图文',2=>'音频',3=>'视频']) ?>
                    <?= $form->field($model, 'type')->radioList([1=>'单课程',2=>'专栏']) ?>
                    <?= $form->field($model, 'cover')->widget(Files::class, [
                        'type' => 'images',
                        'config' => [
                            //'compress'=>true,
                            'pick' => [
                                'multiple' => true,
                            ],
                            // 'formData' => [
                            //      // 不配置则不生成缩略图
                            //     'thumb' => [
                            //         [
                            //             'width' => 600,
                            //             'height' => 400,
                            //         ]
                            //     ]
                            // ]
                        ]
                    ])->hint("列表显示的封面图片。建议大小：  宽600px 高400px"); ?>
                    <?= $form->field($model, 'name') ?>
                        <?= $form->field($productTeacher, 'teacher_id')->widget(Select2::classname(), [  
                        'data' => Teacher::getALL(),
                        'options' => ['placeholder' => '请选择 ...'], 
                        ]); ?>
                    <?= $form->field($model, 'intro')->textarea(['maxlength' => 1024]) ?>
                    <div class="price">
                        <?= $form->field($model, 'real_price')?>
                        <?= $form->field($model, 'market_price')?>
                        <?= $form->field($model, 'vip_price')?>
                        <?= $form->field($model, 'virtual_sale')?>
                    </div>

                    <?= $form->field($model, 'price_type')->radioList([1=>'免费',2=>'vip免费',3=>'购买专栏后免费']) ?>

                    <?= $form->field($model, 'fenxiao_money')->hint("给分销商的金额。需要在【网站设置】开启分销"); ?>
                    <?= $form->field($model, 'show_type')->radioList(['1' => '优先详情', '2' => '优先章节'])->label('详情显示方式') ?>
                    <div style="display: flex;">
                    
                    <?= $form->field($modelData, 'voice',[
                       'options'=>['class' => 'voice content-type'], 
                    ])->widget(Files::class, [
                        'type' => 'voices',
                        'config' => [
                            // 可设置自己的上传地址, 不设置则默认地址
                            // 'server' => '',
                            'pick' => [
                                'multiple' => false,
                            ],
                            'accept' => [
                                'extensions' => ['mp3'],
                                'mimeTypes' => 'audio/*',
                            ],
                        ]
                    ])->hint(""); ?>
                    <?= $form->field($modelData, 'video',[
                       'options'=>['class' => 'video content-type'], 
                    ])->widget(Files::class, [
                        'type' => 'videos',
                        'config' => [
                            // 可设置自己的上传地址, 不设置则默认地址
                            // 'server' => '',
                            'pick' => [
                                'multiple' => false,
                            ],
                            'accept' => [
                                'extensions' => ['mpg', 'mpeg', 'mp4'],
                                'mimeTypes' => 'video/*',
                            ],
                        ]
                    ])->hint(""); ?>
                    
                </div>
                <?= $form->field($model, 'pre_time')->hint("仅视频且使用七牛云存储时有效"); ?>
                    <?= $form->field($modelData, 'content')->widget(\common\widgets\ueditor\UEditor::class) ?>
                    <?= $form->field($model, 'is_top')->checkbox() ?>

                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' :  '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>
<style type="text/css">
.price{display: flex;}
#product-content_type{display: flex;}
#product-content_type .radio{margin-right: 30px;margin: 10px 30px 10px 0;}
#product-type{display: flex;}
#product-type .radio{margin-right: 30px;margin: 10px 30px 10px 0;}
#product-price_type{display: flex;}
#product-price_type .radio{margin-right: 30px;margin: 10px 30px 10px 0;}
</style>