<?php

namespace addons\JhxEdu\merchant\modules\product;

/**
 * Class Module
 * @package addons\JhxEdu\merchant\modules\product
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\merchant\modules\product\controllers';

    public function init()
    {
        parent::init();
    }
}