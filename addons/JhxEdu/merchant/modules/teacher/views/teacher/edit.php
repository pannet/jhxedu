<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\widgets\webuploader\Files;
/* @var $this yii\web\View */
/* @var $model common\models\NavItem */
/* @var $form yii\bootstrap\ActiveForm */
?>
<style type="text/css">
.field-product-show_type{display:flex;align-items: center;}
#product-show_type{display: flex;align-items: center;}
#product-show_type .radio{margin: 0;padding: 0;margin-left: 10px;}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">基本信息</h3>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">
                <div class="col-lg-12">
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'avatar')->widget(Files::class, [
                        'type' => 'images',
                        'config' => [
                            //'compress'=>true,
                            'pick' => [
                                'multiple' => false,
                            ],
                            'formData' => [
                                 // 不配置则不生成缩略图
                                'thumb' => [
                                    [
                                        'width' => 600,
                                        'height' => 660,
                                    ]
                                ]
                            ]
                        ]
                    ])->hint("建议大小：  宽600px 高600px"); ?>
                    <?= $form->field($model, 'level') ?>
                    <?= $form->field($model, 'school') ?>
                    <?= $form->field($model, 'intro')->textarea(['maxlength' => 500]) ?>
                    <?= $form->field($modelData, 'education')->textarea(['maxlength' => 1000]) ?>
                    <?= $form->field($modelData, 'feature')->textarea(['maxlength' => 1000]) ?>
                    <?= $form->field($modelData, 'detail')->widget(\common\widgets\ueditor\UEditor::class) ?>
                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' :  '保存', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>
<style type="text/css">
.price{display: flex;}
.price .form-group{margin-right: 10px;}
</style>