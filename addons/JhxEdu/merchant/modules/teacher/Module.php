<?php

namespace addons\JhxEdu\merchant\modules\teacher;

/**
 * Class Module
 * @package addons\JhxEdu\merchant\modules\teacher
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\merchant\modules\teacher\controllers';

    public function init()
    {
        parent::init();
    }
}