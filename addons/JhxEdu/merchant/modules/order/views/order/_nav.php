<?php

use common\helpers\Url;

$orderStatusMap = [1=>'待支付',2=>'已支付'];
?>

<ul class="nav nav-tabs">
    <li <?php if ($order_status == ''){ ?>class="active"<?php } ?>>
        <a href="<?= Url::to(['index','order_type'=>$order_type]) ?>">全部(<?= $total ?>)</a>
    </li>
    <?php foreach ($orderStatusMap as $key => $item) { ?>
        <li <?php if ($order_status != '' && $order_status == $key){ ?>class="active"<?php } ?>>
            <a href="<?= Url::to(['index', 'order_status' => $key,'order_type'=>$order_type]) ?>"> <?= $item ?></a>
        </li>
    <?php } ?>
</ul>