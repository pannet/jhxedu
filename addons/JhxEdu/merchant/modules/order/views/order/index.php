<?php

use common\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use common\enums\PayTypeEnum;
use common\helpers\ImageHelper;
use addons\JhxEdu\common\enums\ShippingTypeEnum;
use addons\JhxEdu\common\enums\OrderStatusEnum;
use addons\JhxEdu\common\helpers\OrderHelper;
use addons\JhxEdu\common\enums\OrderTypeEnum;
use addons\JhxEdu\common\enums\AccessTokenGroupEnum;

$addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;

$this->title = '订单管理';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<div class="tabs-container">
    <?= $this->render('_nav', [
        'order_status' => $order_status,
        'order_type' => $order_type,
        'total' => $total,
    ]) ?>
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['index', 'order_status' => $order_status]),
                            'method' => 'get',
                        ]); ?>
                        <div class="col-sm-9"></div>
                        <div class="col-sm-3">
                            <div class="input-group m-b">
                                <input type="text" class="form-control" name="order_sn" placeholder="订单编号" value="<?= $order_sn ?>"/>
                                <span class="input-group-btn"><button class="btn btn-white"><i class="fa fa-search"></i> 搜索</button></span>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>订单编号</td>
                            <td>课程封面</td>
                            <th>课程名称</th>
                            <th>价格</th>
                            <th>买家</th>
                            <th>订单来源</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($models as $model) { ?>
                            <?php
                            $rowspanCount = count($model['product']);
                            $rowspanStr = '';
                            $rowspanCount > 0 && $rowspanStr = "rowspan={$rowspanCount}"
                            ?>
                            <tr id= <?= $model->id; ?>>
                                <td>
                                    <?= $model->order_sn; ?>
                                    <div class="pull-right">
                                        下单时间：<?= Yii::$app->formatter->asDatetime($model->created_at) ?>
                                    </div>
                                </td>
                                <td>
                                    <?= ImageHelper::fancyBox($model['product'][0]['picture'])?>
                                </td>
                                <td>
                                    <small><?= $model['product'][0]['name']; ?></small>
                                </td>
                                <td style="text-align: center" <?= $rowspanStr; ?>>
                                    订单金额：<span class="orange"><?= Yii::$app->formatter->asDecimal($model->pay_money, 2); ?></span><br>
                                    <small><?= PayTypeEnum::getValue($model['pay_code']) ?></small>
                                </td>
                                <td <?= $rowspanStr; ?>>
                                    电话：<?= $model->member->mobile; ?><br>
                                    昵称：<?= $model->member->nickname; ?><br>
                                </td>
                                <td <?= $rowspanStr; ?> style="text-align: center">
                                    <span class="blue" style="font-size: 12px"><?= AccessTokenGroupEnum::getValue($model->order_from); ?></span>
                                </td>
                                <td <?= $rowspanStr; ?> style="text-align: center">
                                    <span class="label label-primary">
                                        <?php 
                                            switch ($model['order_status']) {
                                                case '1':
                                                    echo '未支付';
                                                    break;
                                                case '2':
                                                    echo '已支付';
                                                    break;
                                                default:
                                                    # code...
                                                    break;
                                            }
                                        ?> 
                                    </span>
                                </td>
                            </tr>
                            <tr style="background-color: #ecf0f5;"><td colspan="9"></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                                'maxButtonCount' => 5,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var orderProductAgreeUrl = "<?= Url::to(['product/refund-pass']); ?>";
    var orderProductRefuseUrl = "<?= Url::to(['product/refund-no-pass']); ?>";
    var orderProductDeliveryUrl = "<?= Url::to(['product/refund-delivery']); ?>";
    var orderDeliveryUrl = "<?= Url::to(['take-delivery']); ?>";
    var orderCloseUrl = "<?= Url::to(['close']); ?>";
</script>