<?php

namespace addons\JhxEdu\merchant\modules\order;

/**
 * Class Module
 * @package addons\JhxEdu\merchant\modules\order
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'addons\JhxEdu\merchant\modules\order\controllers';

    public function init()
    {
        parent::init();
    }
}