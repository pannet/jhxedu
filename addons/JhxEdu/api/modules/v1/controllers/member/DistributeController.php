<?php

namespace addons\JhxEdu\api\modules\v1\controllers\member;

use Yii;
use yii\data\ActiveDataProvider;
use addons\JhxEdu\common\models\order\DistributeLog;
use api\controllers\UserAuthController;
use common\enums\StatusEnum;

/**
 * 足迹
 *
 * Class FootprintController
 * @package addons\JhxEdu\api\modules\v1\controllers\member
 */
class DistributeController extends UserAuthController
{
    /**
     * @var DistributeLog
     */
    public $modelClass = DistributeLog::class;

    /**
     * 首页
     *
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {

        return new ActiveDataProvider([
            'query' => $this->modelClass::find()
                ->where(['status' => StatusEnum::ENABLED, 'member_id' => Yii::$app->user->identity->member_id])
                ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
                ->with('member')
                ->orderBy('created_at desc')
                ->asArray(),
            'pagination' => [
                'pageSize' => $this->pageSize,
                'validatePage' => false,// 超出分页不返回data
            ],
        ]);
    }
}