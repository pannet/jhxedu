<?php

namespace addons\JhxEdu\api\modules\v1\controllers\member;

use Yii;
use common\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\helpers\ResultHelper;
use common\helpers\AddonHelper;
use common\models\member\Member;
use addons\JhxEdu\common\models\common\MemberVip;
use api\controllers\OnAuthController;

/**
 * 会员相关
 *
 * Class ProductController
 * @package addons\JhxEdu\api\modules\v1\controllers\order
 */
class MemberController extends OnAuthController
{
    /**
     * @var Product
     */
    public $modelClass = Member::class;

    /**
     * 不用进行登录验证的方法
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = [];
    /**
     * 个人中心
     *
     * @return array|null|\yii\data\ActiveDataProvider|\yii\db\ActiveRecord
     */
    public function actionIndex()
    {
        $member_id = Yii::$app->user->identity->member_id;

        $member = $this->modelClass::find()
            ->where(['id' => $member_id])
            ->with('account')
            ->asArray()
            ->one();

        // 优惠券数量
        //$member['coupon_num'] = Yii::$app->tinyShopService->marketingCoupon->findCountByMemberId($member_id);
        // 订单已购课程统计
        $member['buyed_num'] = Yii::$app->jhxEduService->order->getPayCount($member_id);

        // 开启分销商
        // $setting = new SettingForm();
        // $setting->attributes = AddonHelper::getConfig();
        // $member['is_open_commission'] = $setting->is_open_commission;
        // if ($setting->is_open_commission == StatusEnum::ENABLED) {
        //     $member['promoter'] = Yii::$app->tinyDistributionService->promoter->findByMemberId($member_id);
        // }

        return $member;
    }
    /**
     * 会员套餐列表
     * @return mix
     */
    public function actionVipList()
    {
        
        $data = MemberVip::find()->where(['status'=>1])->All();
        return $data;
    }
    /**
     * 会员套餐列表
     * @return mix
     */
    public function actionVipMin()
    {
        
        $data = MemberVip::find()->where(['status'=>1])->orderBy(['price'=>SORT_ASC])->one();
        return $data;
    }
    public function dataCount(){

    }

    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['delete', 'update'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}