<?php

namespace addons\JhxEdu\api\modules\v1\controllers\common;

use Yii;
use api\controllers\OnAuthController;
use addons\JhxEdu\common\models\common\Nav as NavModel;
use addons\JhxEdu\common\enums\AdvLocalEnum;

/**
 * Class AdvController
 * @package addons\JhxEdu\api\modules\v1\controllers\common
 * @author loie
 */
class NavController extends OnAuthController
{
    /**
     * @var Adv
     */
    public $modelClass = Nav::class;

    /**
     * 不用进行登录验证的方法
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = ['item-list'];

	public function actionItemList()
    {
        $data = Yii::$app->request->post();
        $items = NavModel::getItems($data['key']);
        return ['items'=>$items];
    }
    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'delete', 'create', 'update'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}