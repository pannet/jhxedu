<?php

namespace addons\JhxEdu\api\modules\v1\controllers;

use common\helpers\HashidsHelper;
use Yii;
use yii\web\NotFoundHttpException;
use common\helpers\ResultHelper;
use common\helpers\ArrayHelper;
use common\models\member\Member;
use common\models\common\SmsLog;
use common\enums\StatusEnum;
use common\helpers\AddonHelper;
use api\controllers\OnAuthController;
use common\models\member\Auth;
use addons\JhxEdu\api\modules\v1\forms\UpPwdForm;
use addons\JhxEdu\api\modules\v1\forms\LoginForm;
use addons\JhxEdu\api\modules\v1\forms\RefreshForm;
use addons\JhxEdu\api\modules\v1\forms\MobileLogin;
use addons\JhxEdu\api\modules\v1\forms\SmsCodeForm;
use addons\JhxEdu\api\modules\v1\forms\RegisterForm;

/**
 * Class SiteController
 * @package addons\JhxEdu\api\controllers
 * @author jianyan74 <751393839@qq.com>
 */
class SiteController extends OnAuthController
{
    public $modelClass = '';

    /**
     * 不用进行登录验证的方法
     *
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = ['login', 'refresh', 'mobile-login', 'sms-code', 'register', 'up-pwd','config'];

    /**
     * 登录根据用户信息返回accessToken
     *
     * @return array|bool
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        $model->attributes = Yii::$app->request->post();
        if ($model->validate()) {
            return Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group);
        }

        // 返回数据验证失败
        return ResultHelper::json(422, $this->getError($model));
    }

    /**
     * 登出
     *
     * @return array|mixed
     */
    public function actionLogout()
    {
        if (Yii::$app->services->apiAccessToken->disableByAccessToken(Yii::$app->user->identity->access_token)) {
            return ResultHelper::json(200, '退出成功');
        }

        return ResultHelper::json(422, '退出失败');
    }
    /**
     * 配置
     *
     * @return array|mixed
     */
    public function actionConfig()
    {
        $config = AddonHelper::getConfig();
        return $config;
    }

    /**
     * 重置令牌
     *
     * @param $refresh_token
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionRefresh()
    {
        $model = new RefreshForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultHelper::json(422, $this->getError($model));
        }

        return $this->regroupMember(Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group));
    }

    /**
     * 手机验证码登录
     *
     * @return array|mixed
     * @throws \yii\base\Exception
     */
    public function actionMobileLogin()
    {
        $model = new MobileLogin();
        $model->attributes = Yii::$app->request->post();
        if ($model->validate()) {
            return Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group);
            //return $this->regroupMember(Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group));
        }

        // 返回数据验证失败
        return ResultHelper::json(422, $this->getError($model));
    }

    /**
     * 获取验证码
     *
     * @return int|mixed
     * @throws \yii\web\UnprocessableEntityHttpException
     */
    public function actionSmsCode()
    {
        $model = new SmsCodeForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultHelper::json(422, $this->getError($model));
        }

        // 测试
        $code = rand(1000, 9999);
        $log = new SmsLog();
        $log = $log->loadDefaultValues();
        $log->attributes = [
            'mobile' => $model->mobile,
            'code' => $code,
            'member_id' => 0,
            'usage' => $model->usage,
            'error_code' => 200,
            'error_msg' => 'ok',
            'error_data' => '',
        ];
        $log->save();

        return $code;
        // return $model->send();
    }

    /**
     * 注册
     *
     * @return array|mixed
     * @throws \yii\base\Exception
     */
    public function actionRegister()
    {
        $model = new RegisterForm();
        $post = Yii::$app->request->post();
        $model->attributes = $post;
        if (!$model->validate()) {
            return ResultHelper::json(422, $this->getError($model));
        }
        $config = AddonHelper::getConfig();
        $parent = $model->getParent();

        $member = new Member();
        $member->attributes = ArrayHelper::toArray($model);
        $member->promo_code = '';
        $member->merchant_id = !empty($this->getMerchantId()) ? $this->getMerchantId() : 0;
        $member->password_hash = Yii::$app->security->generatePasswordHash($model->password);
        $member->pid = $parent ? $parent->id : 0;
        //暂时先在这里写，避免恶意生成vip信息
        $member->is_vip = 0;
        $member->vip_expire = '';
        if (!$member->save()) {
            return ResultHelper::json(422, $this->getError($member));
        }
        $oauthClient = Yii::$app->request->post('oauth_client');
        $oauthClientUserId = Yii::$app->request->post('oauth_client_user_id');
        
        $auth = $config['register_auth'];
        if($auth == 1 && $oauthClient && $oauthClientUserId){
            $this->authCreate($member->id,$oauthClient,$oauthClientUserId);
        }
        return Yii::$app->services->apiAccessToken->getAccessToken($member, $model->group);
    }
    /**
     * 绑定第三方信息
     *
     * @return array|mixed|\yii\db\ActiveRecord|null
     */
    public  function authCreate($member_id,$oauthClient,$oauthClientUserId)
    {
        /** @var Auth $model */
        if (!($model = Yii::$app->services->memberAuth->findByMemberIdOauthClient($oauthClient, $member_id))) {
            $model = new Auth();
            $model = $model->loadDefaultValues();
            $model->attributes = Yii::$app->request->post();
        }

        if (!$model->isNewRecord && $model->status == StatusEnum::ENABLED) {
            return ResultHelper::json(422, '请先解除该账号绑定');
        }

        $model->oauth_client = $oauthClient;
        $model->oauth_client_user_id = $oauthClientUserId;
        $model->member_id = $member_id;
        $model->status = StatusEnum::ENABLED;
        if (!$model->save()) {
            return ResultHelper::json(422, $this->getError($model));
        }

        // 更改用户信息
        if ($member = Yii::$app->services->member->get($model->member_id)) {
            !$member->head_portrait && $member->head_portrait = $model->head_portrait;
            !$member->gender && $member->gender = $model->gender;
            !$member->nickname && $member->nickname = $model->nickname;
            $member->save();
        }

        return $model;
    }
    /**
     * 密码重置
     *
     * @return array|mixed
     * @throws \yii\base\Exception
     */
    public function actionUpPwd()
    {
        $model = new UpPwdForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultHelper::json(422, $this->getError($model));
        }

        $member = $model->getUser();
        $member->password_hash = Yii::$app->security->generatePasswordHash($model->password);
        if (!$member->save()) {
            return ResultHelper::json(422, $this->getError($member));
        }

        return $this->regroupMember(Yii::$app->services->apiAccessToken->getAccessToken($member, $model->group));
    }


    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['index', 'view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}