<?php

namespace addons\JhxEdu\api\modules\v1\controllers\product;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\helpers\ResultHelper;
use common\enums\StatusEnum;
use common\helpers\AddonHelper;
use addons\JhxEdu\common\models\product\ProductContent;
use api\controllers\OnAuthController;

/**
 * 产品
 *
 * Class ProductController
 * @package addons\JhxEdu\api\modules\v1\controllers\product
 */
class ProductContentController extends OnAuthController
{
    /**
     * @var Product
     */
    public $modelClass = ProductContent::class;

    /**
     * 不用进行登录验证的方法
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = ['index', 'view'];

    /**
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function actionIndex()
    {
        $model = new ProductContent();
        $model->attributes = Yii::$app->request->get();
        $rs = Yii::$app->jhxEduService->productContent->getListBySearch($model);
        return ['contents'=>$rs,'buyed'=>Yii::$app->jhxEduService->product->buyed($model->product_id)];
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        // 判断用户
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        $model = Yii::$app->jhxEduService->productContent->findViewById($id, $member_id);
        
        if (!$model) {
            return ResultHelper::json(422, '该章节找不到了');
        }

        if ($model['status'] == 0) {
            return ResultHelper::json(422, '该章节已下架');
        }
        // 浏览量 + 1
        ProductContent::updateAllCounters(['view' => 1], ['id' => $id]);
        //vip验证
        $member = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member : '';
        $is_vip = Yii::$app->jhxEduService->member->isVip($member);
        /*如果可以设置了免费试看时间，没有购买vip也可以浏览*/
        if($model['price_type'] == 2 && !$is_vip){
            $data=['msg'=>'该章节需要开通vip才能观看','product_id'=>$model['product_id']];
            if($model['type'] == 1){
                return ResultHelper::json(432, $data);
            }else{
                if($model['pre_time'] <= 0){
                    return ResultHelper::json(432,$data);
                }else{
                    //试看内容过滤
                    $model = $this->filter_video($model);
                    return $model;
                }
            }
            
        }
       
        //购买验证
        $buyed = Yii::$app->jhxEduService->order->buyed($model['product_id'],$member_id);

        if($model['price_type'] == 3 && !$buyed){
            $data = ['msg'=>'该章节购买专栏后才能观看','product_id'=>$model['product_id']];
            if($model['type'] == 1){
                //文章没有试看前几秒的逻辑
                return ResultHelper::json(433, $data);
            }else{
                if($model['pre_time'] <= 0){
                    return ResultHelper::json(433, $data);
                }else{
                    //试看内容过滤
                    $model = $this->filter_video($model);
                    return $model;
                }
            }
        }
        return $model;
    }
    private function filter_video($model){
        if($model['video'] && $model['pre_time'] > 0){
            $config =  Yii::$app->params['uploadConfig']['videos'];
            $solt = $config['solt'];
            $names = explode("/videos",$model['video']);
            $path = "videos".$names[1];
            $str = $path.$solt;
            $model['video'] = $names[0]."/".md5($str).".mp4";
        }
        return $model;
    }
    /**
     * 猜你喜欢
     *
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionGuessYouLike()
    {
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        return Yii::$app->tinyShopService->product->getGuessYouLike($member_id);
    }

    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['delete', 'update', 'create'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}