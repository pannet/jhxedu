<?php

namespace addons\JhxEdu\api\modules\v1\controllers\product;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\helpers\ResultHelper;
use common\enums\StatusEnum;
use common\helpers\AddonHelper;
use addons\JhxEdu\common\models\product\Product;
use addons\JhxEdu\common\models\product\ProductStudent;
use addons\JhxEdu\common\models\product\ProductTeacher;
use api\controllers\OnAuthController;

/**
 * 产品
 *
 * Class ProductController
 * @package addons\JhxEdu\api\modules\v1\controllers\product
 */
class ProductController extends OnAuthController
{
    /**
     * @var Product
     */
    public $modelClass = Product::class;

    /**
     * 不用进行登录验证的方法
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = ['index','index-cate','get-by-teacher' ,'view', 'guess-you-like'];

    /**
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function actionIndex()
    {
        $model = new Product();
        $model->attributes = Yii::$app->request->get();
        $model->product_status = 1;
        return Yii::$app->jhxEduService->product->getListBySearch($model);
    }
     /**
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function actionGetByTeacher()
    {
        $model = new ProductTeacher();
        $model->attributes = Yii::$app->request->get();
        return Yii::$app->jhxEduService->product->getByTeacher($model);
    }
    /**
     * 首页分类课程展示
     */
    public function actionIndexCate()
    {
        $cate = Yii::$app->jhxEduService->productCate->findIndexBlock(5);
        $products = [];
        foreach ($cate as $key => $value) {
            $model = new Product();
            $model->cate_id = $value['id'];
            $model->page_size = 6;
            array_push($products,Yii::$app->jhxEduService->product->getListBySearch($model));
        }
        return $products;
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        // 判断用户
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        $model = Yii::$app->jhxEduService->product->findViewById($id, $member_id);
        
        if (!$model) {
            return ResultHelper::json(422, '课程找不到了');
        }

        if ($model['product_status'] == StatusEnum::DISABLED) {
            return ResultHelper::json(422, '课程已下架');
        }
        // 浏览量 + 1
        Product::updateAllCounters(['view' => 1], ['id' => $id]);
        // 足迹
        !empty($member_id) && Yii::$app->jhxEduService->memberFootprint->create($model, $member_id);
        //vip验证
        $member = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member : '';
        $is_vip = Yii::$app->jhxEduService->member->isVip($member);
        $special = false;
        if($member_id){
            $special = productStudent::find()->where(['product_id'=>$id,'member_id'=>$member_id,'type'=>1])->one();
        }
        //如果不是指定学员就按照收费情况判断是否能浏览
        if(!$special){
            /*如果可以设置了免费试看时间，没有购买vip也可以浏览*/
            if($model['price_type'] == 2 && !$is_vip){
                $product = $model;
                unset($product['data']['video']);
                unset($product['data']['voice']);
                $data=['msg'=>'该章节需要开通vip才能观看','id'=>$model['id'],'data'=>$product];
                if($model['content_type'] == 1){
                    return ResultHelper::json(432, $data);
                }else{
                    if($model['pre_time'] <= 0){
                        return ResultHelper::json(432,$data);
                    }else{
                        //试看内容过滤
                        $model = $this->filter_video($model);
                        return $model;
                    }
                }
                
            }
           
            //购买验证
            $buyed = Yii::$app->jhxEduService->order->buyed($model['id'],$member_id);

            if($model['price_type'] == 3 && !$buyed){
                $product = $model;
                unset($product['data']['video']);
                unset($product['data']['voice']);
                $data = ['msg'=>'该章节购买专栏后才能观看','id'=>$model['id'],'data'=>$product];
                if($model['type'] == 1){
                    //文章没有试看前几秒的逻辑
                    return ResultHelper::json(433, $data);
                }else{
                    if($model['pre_time'] <= 0){
                        return ResultHelper::json(433, $data);
                    }else{
                        //试看内容过滤
                        $model = $this->filter_video($model);
                        return $model;
                    }
                }
            }
        }
        
        // 评论
        if (!empty($model['evaluate'])) {
            foreach ($model['evaluate'] as &$datum) {
                empty($datum['again_covers']) && $datum['again_covers'] = [];
                !is_array($datum['again_covers']) && $datum['again_covers'] = Json::decode($datum['again_covers']);
                empty($datum['covers']) && $datum['covers'] = [];
                !is_array($datum['covers']) && $datum['covers'] = Json::decode($datum['covers']);
                // 匿名
                if ($datum['is_anonymous'] == StatusEnum::ENABLED) {
                    $datum['member_id'] = '';
                    $datum['member_nickname'] = '';
                    $datum['member_head_portrait'] = '';
                }
            }
        }
        return $model;
    }

    /**
     * 猜你喜欢
     *
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionGuessYouLike()
    {
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        return Yii::$app->tinyShopService->product->getGuessYouLike($member_id);
    }

    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['delete', 'update', 'create'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}