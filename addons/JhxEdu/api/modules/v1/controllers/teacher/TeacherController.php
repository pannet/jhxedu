<?php

namespace addons\JhxEdu\api\modules\v1\controllers\teacher;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\helpers\ResultHelper;
use common\enums\StatusEnum;
use common\helpers\AddonHelper;
use addons\JhxEdu\common\models\teacher\Teacher;
use api\controllers\OnAuthController;

/**
 * 产品
 *
 * Class ProductController
 * @package addons\JhxEdu\api\modules\v1\controllers\product
 */
class TeacherController extends OnAuthController
{
    /**
     * @var Product
     */
    public $modelClass = Teacher::class;

    /**
     * 不用进行登录验证的方法
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $authOptional = ['index','index-cate', 'view', 'guess-you-like'];

    /**
     * @return array|ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function actionIndex()
    {
        $model = new Teacher();
        $model->attributes = Yii::$app->request->get();
        $model->status = 1;
        return Yii::$app->jhxEduService->teacher->getListBySearch($model);
    }
    
    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($id)
    {
        // 判断用户
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        $model = Yii::$app->jhxEduService->teacher->findViewById($id, $member_id);
        
        if (!$model) {
            return ResultHelper::json(422, '老师找不到了');
        }

        if ($model['status'] == StatusEnum::DISABLED) {
            return ResultHelper::json(422, '老师已被禁用或删除');
        }
        return $model;
    }

    /**
     * 猜你喜欢
     *
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionGuessYouLike()
    {
        $member_id = !Yii::$app->user->isGuest ? Yii::$app->user->identity->member_id : '';

        return Yii::$app->tinyShopService->product->getGuessYouLike($member_id);
    }

    /**
     * 权限验证
     *
     * @param string $action 当前的方法
     * @param null $model 当前的模型类
     * @param array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['delete', 'update', 'create'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}